(function () {
	var settings = null;

	mybookprogress.on('models_loaded', function() {
		settings = mybookprogress.settings;
	});

	var View = mybookprogress.View;

	mybookprogress.SirWalterView = View.extend({
		events: {
			'click .mbp-sw-speech-response': 'respond',
		},

		initialize: function() {
			this.state = _.clone(settings.get('sir_walter_state')) || {};
			this.show_greeting();
		},

		find_new_index: function(list, last_index) {
			do { var index = Math.floor(Math.random()*list.length); } while(index === last_index);
			return index;
		},

		save_state: function() {
			settings.set('sir_walter_state', _.clone(this.state));
		},

		show_greeting: function() {
			var greeting_num = this.find_new_index(mybookprogress.sir_walter_text.greetings, this.state.last_greeting);
			var greeting = mybookprogress.sir_walter_text.greetings[greeting_num];

			this.state.last_greeting = greeting_num;
			this.save_state();

			this.show_text(greeting.text, greeting.responses);
		},

		show_random: function() {
			var random_text_num = this.find_new_index(mybookprogress.sir_walter_text.random_texts, this.state.last_random_text);
			var random_text = mybookprogress.sir_walter_text.random_texts[random_text_num];

			var random_response_num = this.find_new_index(mybookprogress.sir_walter_text.random_responses, this.state.last_random_response);
			var random_response = mybookprogress.sir_walter_text.random_responses[random_response_num];

			this.state.last_random_text = random_text_num;
			this.state.last_random_response = random_response_num;
			this.save_state();

			this.show_text(random_text, [random_response]);
		},

		respond: function() {
			this.show_random();
		},

		show_text: function(text, responses) {
			if(!_.isString(text)) { text = ''; }
			if(!_.isArray(responses)) { responses = []; }
			this.text = text;
			this.responses = responses;
			this.render();
		},

		render: function() {
			var new_speech = '<div class="mbp-sw-speech-text"><div class="mbp-sw-speech-text-inner">'+this.text+'</div></div>';
			if(this.responses.length) {
				new_speech += '<div class="mbp-sw-speech-responses">';
				for(i in this.responses) {
					new_speech += '<div class="mbp-sw-speech-response">'+this.responses[i]+'</div>';
				}
				new_speech += '</div>';
			}
			this.$('.mbp-sw-speech').empty().append(jQuery(new_speech));
			return this;
		},
	});

	mybookprogress.sir_walter_text = {
		greetings: [
			{
				text: '<p>Good day, my dear friend!</p><p>Hast thou made progress on thine book lately?</p>',
				responses: ['Good day, sir!'],
			},
			{
				text: '<p>Delightful to see you again!</p><p>You do look splendid today, if I may be so bold.</p>',
				responses: ['Why, thank you!'],
			},
			{
				text: '<p>Hast thou made progress on thine book? Jolly good!</p>',
				responses: ['Indeed, good sir!'],
			},
		],
		random_texts: [
			'<p>There is no such thing as an &#8220;aspiring writer.&#8221; If you write you are a writer.</p>',
			'<p>Don\'t fear writing. Fear <em>not writing</em>, a vile beast indeed. You slay that monster every time you lay pen to paper.</p>',
			'<p>Write through the pain. Readers connect with pain better than they do with perfection.</p>',
			'<p class="mbp-small-text">&#8220;Keep writing. Keep doing it and doing it. Even in the moments when it\'s so hurtful to think about writing.&#8221;</p><p class="mbp-small-text">&#126;Don Draper</p>',
			'<p>&#8220;You can\'t wait for inspiration. You have to go after it with a club.&#8221;</p><p>&#126;Jack London</p>',
			'<p class="mbp-small-text">&#8220;If you want to be a writer, you must do two things above all others: read a lot and write a lot.&#8221;</p><p class="mbp-small-text">&#126;Stephen King</p>',
			'<p>&#8220;A professional writer is an amateur who didn\'t quit.&#8221;</p><p>&#126;Richard Bach</p>',
			'<p>&#8220;Writing is an act of faith, not a trick of grammar.&#8221;</p><p>&#126;E. B. White</p>',
			'<p class="mbp-small-text">&#8220;Opportunities are usually disguised as hard work, so most people don\'t recognize them.&#8221;</p><p class="mbp-small-text">&#126;Ann Landers</p>',
			'<p class="mbp-small-text">&#8220;If my doctor told me I had only six minutes to live, I wouldn\'t brood. I\'d type a little faster.&#8221;</p><p class="mbp-small-text">&#126;Isaac Asimov</p>',
			'<p>&#8220;You don\'t make art out of good intentions.&#8221;</p><p>&#126;Gustave Flaubert</p>',
			'<p>&#8220;Only a mediocre person is always at his best.&#8221;</p><p>&#126;W. Somerset Maugham</p>',
			'<p>&#8220;Make it simple but significant.&#8221;</p><p>&#126;Don Draper</p>',
			'<p class="mbp-small-text">&#8220;Working on the right thing is usually more important than working hard.&#8221;</p><p class="mbp-small-text">&#126;Caterina Fake</p>',
			'<p>&#8220;Creativity is a drug I cannot live without.&#8221;</p><p>&#126;Cecil B. DeMille</p>',
		],
		random_responses: [
			'Go on, sir&hellip;',
			'Tell me more&hellip;',
			'You don\'t say!',
		],
	};

})();
