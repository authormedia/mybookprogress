(function () {
	var settings = null;

	mybookprogress.on('models_loaded', function() {
		settings = mybookprogress.settings;
	});

	var View = mybookprogress.View;

	mybookprogress.DisplayTabView = View.extend({
		className: 'mbp-admin-tab',
		template: mybookprogress.utils.template(jQuery('#display_tab_template').html()),

		events: {
			'change .mbp-widget-sidebar': 'render_button',
			'click .mbp-widget-button': 'clicked_widget_button',
			'click .mbp-alert-message': 'clicked_mailinglist_warning',
		},

		initialize: function() {
			settings.on('change:mailinglist_type', this.render_mailinglist_warning, this);
			settings.on('change:mailchimp_list', this.render_mailinglist_warning, this);
			settings.on('change:other_subscribe_url', this.render_mailinglist_warning, this);
		},

		render: function() {
			this.$el.html(this.template());
			this.render_button();
			this.render_mailinglist_warning();
			return this;
		},

		render_button: function() {
			var has_widget = this.$('.mbp-widget-sidebar option:selected').attr('data-has-widget');
			if(has_widget === 'yes') {
				this.$('.mbp-widget-button').addClass('mbp-edit-widget');
				this.$('.mbp-widget-button').html(mybookprogress_i18n.edit_widget);
			} else {
				this.$('.mbp-widget-button').removeClass('mbp-edit-widget');
				this.$('.mbp-widget-button').html(mybookprogress_i18n.add_widget);
			}
		},

		render_mailinglist_warning: function() {
			if(settings.get('mailinglist_type') === 'mailchimp') {
				if(!settings.get('mailchimp_list')) {
					this.$('.mbp-alert-message-container').show();
				} else {
					this.$('.mbp-alert-message-container').hide();
				}
			} else if(settings.get('mailinglist_type') === 'other') {
				if(!settings.get('other_subscribe_url')) {
					this.$('.mbp-alert-message-container').show();
				} else {
					this.$('.mbp-alert-message-container').hide();
				}
			} else {
				this.$('.mbp-alert-message-container').show();
			}
		},

		clicked_widget_button: function() {
			if(this.$('.mbp-widget-button').hasClass('mbp-edit-widget')) {
				return true;
			} else if(!this.$('.mbp-widget-button').hasClass('mbp-disabled')) {
				this.$('.mbp-widget-button').addClass('mbp-disabled');
				this.$('.mbp-widget-sidebar').attr('disabled', 'disabled');

				mybookprogress.WPQuery('mbp_add_sidebar', {sidebar: this.$('.mbp-widget-sidebar').val()}).then(_.bind(function(response) {
					this.$('.mbp-widget-button').removeClass('mbp-disabled');
					this.$('.mbp-widget-sidebar').removeAttr('disabled');
					this.$('.mbp-widget-sidebar option:selected').attr('data-has-widget', 'yes');
					this.render_button();
				}, this));
			}
			return false;
		},

		clicked_mailinglist_warning: function() {
			jQuery('.mbp-admin-tabs > .ui-tabs-nav a[href="#mbp-promote-tab"]').click();
		},
	});
})();
