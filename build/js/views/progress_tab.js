(function () {
	var settings = null;
	var books = null;

	mybookprogress.on('models_loaded', function() {
		settings = mybookprogress.settings;
		books = mybookprogress.books;
	});

	mybookprogress.ProgressTabView = mybookprogress.View.extend({
		className: 'mbp-admin-tab',

		currentview: null,
		currentbook: null,
		inbooksetup: false,

		initialize: function() {
			mybookprogress.on('loaded', function() {
				if(mybookprogress.utils.get_query_var('book')) {
					if(this.show_book(mybookprogress.utils.get_query_var('book'))) { return; }
				}
				if(mybookprogress.utils.get_query_var('books')) {
					var books = mybookprogress.utils.get_query_var('books').split(',');
					if(books.length && this.show_book(parseInt(books[0]))) { return; }
				}
				this.show_current_book();
			}, this);
		},

		toggle_setup: function() {
			if(!this.currentbook) { return; }
			if(this.inbooksetup) {
				this.change_view(new mybookprogress.BookProgressView({model:this.currentbook}));
				this.inbooksetup = false;
			} else {
				this.change_view(new mybookprogress.BookSetupView({model:this.currentbook}));
				this.inbooksetup = true;
			}
		},

		render: function() {
			this.$el.empty();
			if(this.currentview) { this.$el.append(this.currentview.render().$el); }
			return this;
		},

		change_view: function(view) {
			if(this.currentview) { this.currentview.remove(); }
			this.currentview = view;
			this.render();
		},

		show_overview: function() {
			this.currentbook = null;
			this.change_view(new mybookprogress.OverviewView());
			this.update_current_book();
		},

		show_book: function(book_id) {
			book_id = typeof book_id === 'number' ? book_id : (typeof book_id === 'string' ? parseInt(book_id) : 0);
			var book = null;
			if(book_id > 0) {
				var new_book = books.get(book_id);
				if(!new_book) { return false; }
				this.currentbook = new_book;
				this.update_current_book();
				this.change_view(new mybookprogress.BookProgressView({model:this.currentbook}));
				this.inbooksetup = false;
			} else {
				this.currentbook = new mybookprogress.Book();
				this.change_view(new mybookprogress.BookSetupView({model:this.currentbook}));
				this.inbooksetup = true;
			}
			return true;
		},

		show_current_book: function() {
			var current_book = this.get_current_book();
			if(current_book == 'overview') {
				this.show_overview();
			} else {
				this.show_book(current_book);
			}
		},

		update_current_book: function() {
			if(this.currentbook) {
				settings.set('current_book', this.currentbook.id);
			} else {
				settings.set('current_book', 'overview');
			}
		},

		get_current_book: function() {
			var val = settings.get('current_book');
			if(val !== 'overview' && !books.get(val)) {
				if(books.length > 0) {
					val = books.at(0).id;
					settings.set('current_book', val);
				} else {
					val = -1;
					settings.set('current_book', val);
				}
			}
			return val;
		},
	});
})();
