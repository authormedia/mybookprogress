(function () {
	var settings = null;
	var utils = mybookprogress.utils;

	mybookprogress.on('models_loaded', function() {
		settings = mybookprogress.settings;
	});

	var View = mybookprogress.View;

	mybookprogress.PromoteTabView = View.extend({
		className: 'mbp-admin-tab',
		template: mybookprogress.utils.template(jQuery('#promote_tab_template').html()),
		initialize: function() {
			this.set_subview('.mbp-mailinglist-section', new mybookprogress.SetupMailinglistView());
			this.set_subview('.mbp-mybooktable-section', new mybookprogress.SetupMyBookTableView());
			this.set_subview('.mbp-linkback-button-container', new mybookprogress.SetupLinkbackView());
		}
	});

	mybookprogress.SetupMyBookTableView = View.extend({
		template: mybookprogress.utils.template(jQuery('#setup_mybooktable_template').html()),
		events: {
			'change .mbp-mybooktable-social-media-link': 'changed_mybooktable_social_media_link',
			'change .mbp-mybooktable-frontend-link': 'changed_mybooktable_frontend_link',
			'click .mbp-install-mybooktable-button': 'install_mybooktable',
		},
		render: function() {
			View.prototype.render.call(this);
			if(!settings.get('mybooktable_installed')) { this.$el.addClass('mbp-disabled'); } else { this.$el.removeClass('mbp-disabled'); }
			this.$('.mbp-mybooktable-social-media-link').prop('checked', settings.get('mybooktable_social_media_link'));
			this.$('.mbp-mybooktable-frontend-link').prop('checked', settings.get('mybooktable_frontend_link'));
			return this;
		},
		changed_mybooktable_social_media_link: function() {
			settings.set('mybooktable_social_media_link', this.$('.mbp-mybooktable-social-media-link').is(':checked'));
		},
		changed_mybooktable_frontend_link: function() {
			settings.set('mybooktable_frontend_link', this.$('.mbp-mybooktable-frontend-link').is(':checked'));
		},
		install_mybooktable: function() {
			var content = jQuery('<iframe></iframe>');
			content.attr('src', this.$('.mbp-install-mybooktable-button').attr('data-mbp-link'));
			content.css({width: '100%', height: '99%'});
			utils.modal(content, {width: 800, height: '90%'});
		}
	});

	mybookprogress.SetupMailChimpView = View.extend({
		template: mybookprogress.utils.template(jQuery('#setup_mailchimp_template').html()),

		events: {
			'click .mbp-mailchimp-apikey-setting .mbp-setting-feedback': 'clicked_feedback',
			'input .mbp-mailchimp-apikey': 'changed_apikey',
			'change .mbp-mailchimp-list': 'changed_mailing_list',
		},

		initialize: function() {
			this.verify_apikey();
		},

		render: function() {
			this.$el.html(this.template());
			this.$('.mbp-mailchimp-apikey').val(settings.get('mailchimp_apikey'));
			this.render_controls();
			return this;
		},

		render_controls: function() {
			this.$('.mbp-mailchimp-apikey').attr('class', 'mbp-mailchimp-apikey');
			this.$('.mbp-mailchimp-apikey').removeAttr('disabled');
			this.$('.mbp-mailchimp-apikey-setting .mbp-setting-feedback').attr('class', 'mbp-setting-feedback');
			if(this.apikey_status == 'checking') {
				this.$('.mbp-mailchimp-apikey').attr('disabled', 'disabled');
				this.$('.mbp-mailchimp-apikey-setting .mbp-setting-feedback').addClass('checking');
			} else if(this.apikey_status == 'good') {
				this.$('.mbp-mailchimp-apikey').addClass('mbp-correct');
				this.$('.mbp-mailchimp-apikey-setting .mbp-setting-feedback').addClass('good');
			} else if(this.apikey_status == 'bad') {
				this.$('.mbp-mailchimp-apikey').addClass('mbp-error');
				this.$('.mbp-mailchimp-apikey-setting .mbp-setting-feedback').addClass('bad');
			} else if(this.apikey_status == 'editing') {
				this.$('.mbp-mailchimp-apikey-setting .mbp-setting-feedback').addClass('refresh');
			} else if(this.apikey_status == 'empty') {
				this.$('.mbp-mailchimp-apikey-setting .mbp-setting-feedback').addClass('help');
			}

			this.$('.mbp-mailchimp-list').attr('class', 'mbp-mailchimp-list');
			this.$('.mbp-mailchimp-list').removeAttr('disabled');
			this.$('.mbp-mailchimp-list-setting .mbp-setting-feedback').attr('class', 'mbp-setting-feedback');
			this.$('.mbp-mailchimp-list-setting .mbp-setting-feedback .mbp-setting-feedback-text').empty();
			if(this.apikey_status == 'good') {
				if(this.mailing_lists_status == 'checking') {
					this.$('.mbp-mailchimp-list').attr('disabled', 'disabled');
					this.$('.mbp-mailchimp-list').html('<option> '+mybookprogress_i18n.loading+'... </option>');
					this.$('.mbp-mailchimp-list-setting .mbp-setting-feedback').addClass('checking');
				} else {
					var mailchimp_list = settings.get('mailchimp_list');
					if(this.mailing_lists.length == 0) {
						this.$('.mbp-mailchimp-list').attr('disabled', 'disabled');
						this.$('.mbp-mailchimp-list').html('<option> -- '+mybookprogress_i18n.no_lists_available+' -- </option>');
						this.$('.mbp-mailchimp-list-setting .mbp-setting-feedback').addClass('none');
					} else {
						this.$('.mbp-mailchimp-list').empty();
						if(!mailchimp_list) {
							this.$('.mbp-mailchimp-list').html('<option value=""> -- '+mybookprogress_i18n.choose_one+' -- </option>');
							this.$('.mbp-mailchimp-list').addClass('mbp-error');
							this.$('.mbp-mailchimp-list-setting .mbp-setting-feedback').addClass('bad');
						} else {
							var subscribers = this.mailing_list.stats.member_count;
							this.$('.mbp-mailchimp-list').addClass('mbp-correct');
							this.$('.mbp-mailchimp-list-setting .mbp-setting-feedback .mbp-setting-feedback-text').html(subscribers > 0 ? subscribers+' '+mybookprogress_i18n.subscribers : '');
							this.$('.mbp-mailchimp-list-setting .mbp-setting-feedback').addClass('good');
						}
						for(var i = this.mailing_lists.length - 1; i >= 0; i--) {
							var is_current_list = this.mailing_lists[i].id === mailchimp_list;
							this.$('.mbp-mailchimp-list').append('<option value="'+this.mailing_lists[i].id+'"'+(is_current_list ? ' selected="selected"' : '')+'>'+this.mailing_lists[i].name+'</option>');
						}
					}
				}
			} else {
				this.$('.mbp-mailchimp-list').attr('disabled', 'disabled');
				this.$('.mbp-mailchimp-list').html('<option> -- '+mybookprogress_i18n.no_lists_available+' -- </option>');
				this.$('.mbp-mailchimp-list-setting .mbp-setting-feedback').addClass('none');
			}
		},

		clicked_feedback: function() {
			if(this.apikey_status == 'editing') {
				settings.set({mailchimp_apikey: this.$('.mbp-mailchimp-apikey').val(), mailchimp_list: '', mailchimp_subscribe_url: ''});
				this.verify_apikey();
			} else if(this.apikey_status == 'empty') {
				window.open('http://kb.mailchimp.com/accounts/management/about-api-keys', '_blank').focus();
			}
		},

		changed_apikey: function() {
			this.apikey_status = 'editing';
			this.render_controls();
		},

		changed_mailing_list: function() {
			var mailchimp_list = this.$('.mbp-mailchimp-list').val();
			var mailchimp_subscribe_url = settings.get('mailchimp_subscribe_url');
			for(var i = this.mailing_lists.length - 1; i >= 0; i--) {
				if(this.mailing_lists[i].id == mailchimp_list) {
					mailchimp_subscribe_url = this.mailing_lists[i].subscribe_url_long;
					this.mailing_list = this.mailing_lists[i];
				}
			}
			settings.set({mailchimp_list: mailchimp_list, mailchimp_subscribe_url: mailchimp_subscribe_url});
			this.render_controls();
		},

		verify_apikey: function() {
			if(settings.get('mailchimp_apikey') === '') {
				this.apikey_status = 'empty';
			} else {
				this.apikey_status = 'checking';
				utils.do_mailchimp_query('helper/ping').done(_.bind(this.verified_apikey, this));
			}
			this.render_controls();
		},

		verified_apikey: function(response) {
			var verified = response && response.msg;
			this.apikey_status = verified ? 'good' : 'bad';
			if(this.apikey_status == 'good') {
				this.mailing_lists_status = 'checking';
				utils.do_mailchimp_query('lists/list').done(_.bind(this.fetched_mailing_lists, this));
			} else if(settings.get('mailchimp_list')) {
				settings.set({mailchimp_list: '', mailchimp_subscribe_url: ''});
			}
			this.render_controls();
		},

		fetched_mailing_lists: function(response) {
			this.mailing_lists_status = 'done';
			if(!response || response.error || !response.data || !_.isArray(response.data)) {
				this.mailing_lists = [];
				settings.set({mailchimp_list: '', mailchimp_subscribe_url: ''});
			} else {
				this.mailing_lists = response.data;
				if(settings.get('mailchimp_list')) {
					var found_current_list = false;
					for(var i = this.mailing_lists.length - 1; i >= 0; i--) {
						if(this.mailing_lists[i].id == settings.get('mailchimp_list')) {
							found_current_list = true;
							if(this.mailing_lists[i].subscribe_url_long !== settings.get('mailchimp_subscribe_url')) {
								settings.set('mailchimp_subscribe_url', this.mailing_lists[i].subscribe_url_long);
							}
							this.mailing_list = this.mailing_lists[i];
							break;
						}
					}
					if(!found_current_list) {
						settings.set({mailchimp_list: '', mailchimp_subscribe_url: ''});
					}
				}
			}
			this.mailing_lists_status = settings.get('mailchimp_list') === '' ? 'bad' : 'good';
			this.render_controls();
		},
	});

	mybookprogress.SetupOtherView = View.extend({
		template: mybookprogress.utils.template(jQuery('#setup_other_template').html()),

		events: {
			'input .mbp-other-mailinglist-url': 'update_mailinglist',
			'blur .mbp-other-mailinglist-url': 'save_mailinglist',
			'click .mbp-other-mailinglist-setting .mbp-setting-feedback': 'clicked_feedback',
		},

		initialize: function() {
			this.editing = false;
		},

		render: function() {
			this.$el.html(this.template());
			this.$('.mbp-other-mailinglist-url').val(settings.get('other_subscribe_url'));
			this.render_feedback();
			return this;
		},

		update_mailinglist: function() {
			if(this.update_timer) { clearTimeout(this.update_timer); }
			this.update_timer = setTimeout(_.bind(this.save_mailinglist, this), 1000);
			this.editing = true;
			this.render_feedback();
		},

		save_mailinglist: function() {
			if(this.update_timer) { clearTimeout(this.update_timer); this.update_timer = null; }
			settings.set('other_subscribe_url', this.$('.mbp-other-mailinglist-url').val());
			this.editing = false;
			this.render_feedback();
		},

		clicked_feedback: function() {
			if(this.$('.mbp-other-mailinglist-setting .mbp-setting-feedback').hasClass('help')) {
				window.open('http://www.google.com/', '_blank').focus();
			}
		},

		render_feedback: function() {
			// url matching regex from http://codegolf.stackexchange.com/questions/464/shortest-url-regex-match-in-javascript
			var url_regex = /^https?:\/\/[\w-]+(\.[\w-]+)+\.?(:\d+)?(\/\S*)?$/i;

			var url = settings.get('other_subscribe_url') || '';
			this.$('.mbp-other-mailinglist-url').attr('class', 'mbp-other-mailinglist-url')
			this.$('.mbp-other-mailinglist-setting .mbp-setting-feedback').attr('class', 'mbp-setting-feedback');
			if(this.editing) {
				this.$('.mbp-other-mailinglist-setting .mbp-setting-feedback').addClass('checking');
			} else if(url == '') {
				this.$('.mbp-other-mailinglist-setting .mbp-setting-feedback').addClass('help');
			} else if(url.match(url_regex)) {
				this.$('.mbp-other-mailinglist-setting .mbp-setting-feedback').addClass('good');
				this.$('.mbp-other-mailinglist-url').addClass('mbp-correct');
			} else {
				this.$('.mbp-other-mailinglist-setting .mbp-setting-feedback').addClass('bad');
				this.$('.mbp-other-mailinglist-url').addClass('mbp-error');
			}
		},
	});

	mybookprogress.SetupMailinglistView = View.extend({
		template: mybookprogress.utils.template(jQuery('#setup_mailinglist_template').html()),

		events: {
			'click .mbp-mailinglist-type-button': 'update_mailinglist_type',
		},

		mailinglist_types: {
			mailchimp: {
				name: mybookprogress_i18n.mailchimp,
				view: mybookprogress.SetupMailChimpView,
			},
			other: {
				name: mybookprogress_i18n.other,
				view: mybookprogress.SetupOtherView,
			},
		},

		initialize: function() {
			settings.on('change:mailinglist_type', this.render_mailinglist_types, this);
		},

		render: function() {
			this.$el.html(this.template());
			this.render_mailinglist_types();
			return this;
		},

		render_mailinglist_types: function() {
			var current_type = settings.get('mailinglist_type');
			this.$('.mbp-mailinglist-types').empty();
			for(type in this.mailinglist_types) {
				var new_button = jQuery('<div class="mbp-setting-button mbp-mailinglist-type-button"></div>');
				new_button.attr('data-mbp-mailinglist-type', type);
				new_button.text(this.mailinglist_types[type].name);
				if(type === current_type) { new_button.addClass('mbp-selected'); }
				this.$('.mbp-mailinglist-types').append(new_button);
			}
			if(current_type in this.mailinglist_types) {
				this.set_subview('.mbp-mailinglist-setup', new this.mailinglist_types[current_type].view());
			}
		},

		update_mailinglist_type: function(e) {
			settings.set('mailinglist_type', jQuery(e.target).attr('data-mbp-mailinglist-type'));
		},
	});

	mybookprogress.SetupLinkbackView = View.extend({
		template: mybookprogress.utils.template(jQuery('#setup_linkback_template').html()),

		events: {
			'click .mbp-linkback-button': 'clicked',
			'mouseleave .mbp-linkback-button': 'reset',
		},

		render: function() {
			this.$el.html(this.template());
			if(!settings.get('enable_linkback')) { this.$('.mbp-linkback-heart').addClass('broken'); }
			this.set_message(settings.get('enable_linkback') ? 'enabled' : 'disabled', false);
			return this;
		},

		set_message: function(message, animate) {
			if(typeof animate === 'undefined') { animate = true; }

			var old_messages = this.$('.mbp-linkback-messages .mbp-linkback-message:not(.mbp-hidden)');
			if(animate) {
				old_messages.stop().animate({opacity: 0}, {duration: 500}).addClass('mbp-hidden');
			} else {
				old_messages.css({opacity: 0}).addClass('mbp-hidden');
			}

			var shown_message = this.$('.mbp-linkback-message-'+message);
			shown_message.removeClass('mbp-hidden');
			if(animate) {
				shown_message.stop().animate({opacity: 1}, {duration: 500});
			} else {
				shown_message.css({opacity: 1});
			}
		},

		clicked: function() {
			if(this.$('.mbp-linkback-button').hasClass('inactive')) { return; }
			settings.set('enable_linkback', !settings.get('enable_linkback'));
			this.$('.mbp-enable-linkback').prop("checked", settings.get('enable_linkback'));
			this.$('.mbp-linkback-button').addClass('inactive');
			if(settings.get('enable_linkback')) {
				this.$('.mbp-linkback-heart').removeClass('broken');
				this.set_message('enable');
				if(this.linkback_timer) { window.clearTimeout(this.linkback_timer); }
				this.linkback_timer = window.setTimeout(_.bind(function() {
					this.set_message('enabled');
					this.linkback_timer = null;
				}, this), 2000);
			} else {
				this.$('.mbp-linkback-heart').addClass('broken');
				this.set_message('disable');
				if(this.linkback_timer) { window.clearTimeout(this.linkback_timer); }
				this.linkback_timer = window.setTimeout(_.bind(function() {
					this.set_message('disabled');
					this.linkback_timer = null;
				}, this), 2000);
			}
		},

		reset: function() {
			this.$('.mbp-linkback-button').removeClass('inactive');
		},
	});
})();
