(function () {
	var settings = null;
	var utils = mybookprogress.utils;

	mybookprogress.on('models_loaded', function() {
		settings = mybookprogress.settings;
	});

	var View = mybookprogress.View;

	mybookprogress.HelpView = View.extend({
		template: mybookprogress.utils.template(jQuery('#help_template').html()),

		events: {
			'click .mbp-help-button': 'toggle_help',
			'click .mbp-help-box-close': 'toggle_help',
		},

		initialize: function() {
			this.show_search();
		},

		render: function() {
			View.prototype.render.call(this);
			return this;
		},

		toggle_help: function() {
			if(this.$('.mbp-help-box').is(':visible')) {
				this.$('.mbp-help-box').hide(300);
			} else {
				this.$('.mbp-help-box').show(300);
				this.show_search();
			}
		},

		show_topic: function(topic, ref_data) {
			var topic = mybookprogress.help_topics.get(topic);
			if(topic) {
				if(typeof ref_data === 'string') { ref_data = {type: 'string', string: ref_data}; }

				//check for refrence loops
				if(ref_data.ref) {
					var ref = ref_data.ref;
					while(ref) {
						if(ref.type === 'topic' && ref.topic === topic.slug) {
							ref_data = ref.ref;
						}
						ref = ref.ref;
					}
				}

				this.set_subview('.mbp-help-box-content', new mybookprogress.HelpTopicView({topic: topic, ref_data: ref_data}));
			}
		},

		show_search: function(search) {
			this.set_subview('.mbp-help-box-content', new mybookprogress.HelpSearchView({search: search}));
		},
	});

	mybookprogress.HelpSearchView = View.extend({
		template: mybookprogress.utils.template(jQuery('#help_search_template').html()),

		events: {
			'input .mbp-help-search': 'update_search',
			'click .mbp-help-topic-link': 'topic_clicked',
		},

		initialize: function(options) {
			this.search = '';
			if(options && options.search) { this.search = options.search; }
		},

		render: function() {
			View.prototype.render.call(this);
			this.$('.mbp-help-search').val(this.search);
			this.render_search();
			this.render_common();
		},

		topic_clicked: function(e) {
			this.track_search();
			if(jQuery(e.target).parents('.mbp-help-common').length) {
				mybookprogress.help.show_topic(jQuery(e.target).attr('data-mbp-help-topic'), 'common-topics');
			} else {
				mybookprogress.help.show_topic(jQuery(e.target).attr('data-mbp-help-topic'), {type: 'search', search: this.search});
			}
		},

		update_search: function() {
			this.search = this.$('.mbp-help-search').val();
			this.render_search();
			this.schedule_track_search();
		},

		render_search: function() {
			if(this.search) {
				var help_topics = [];
				for(slug in mybookprogress.help_topics) {
					help_topics.push(mybookprogress.help_topics.get(slug));
				}

				try {
					var search_topics = new Fuse(help_topics, {
						keys: ['title', 'keywords'],
					}).search(this.search);
					search_topics = _.first(search_topics, 6);
				} catch(e) {
					search_topics = [];
				}

				this.$('.mbp-help-search-results ul').empty();
				if(search_topics.length) {
					_.each(search_topics, function(topic) {
						this.$('.mbp-help-search-results ul').append(jQuery('<li><span class="mbp-help-topic-link" data-mbp-help-topic="'+topic.slug+'">'+topic.title+'</span></li>'));
					}, this);
				} else {
					this.$('.mbp-help-search-results ul').append(jQuery('<li class="no-results">No results</li>'));
				}

				if(this.$('.mbp-help-common').is(':visible')) { this.$('.mbp-help-common').hide(300); }
			}
		},

		render_common: function() {
			var common_topics = [];
			for(slug in mybookprogress.help_topics) {
				if(mybookprogress.help_topics.get(slug).common === true) {
					common_topics.push(mybookprogress.help_topics.get(slug));
				}
			}

			this.$('.mbp-help-common ul').empty();
			_.each(common_topics, function(topic) {
				this.$('.mbp-help-common ul').append(jQuery('<li><span class="mbp-help-topic-link" data-mbp-help-topic="'+topic.slug+'">'+topic.title+'</span></li>'));
			}, this);
		},

		schedule_track_search: function() {
			if(this.track_search_timer) { clearTimeout(this.track_search_timer); }
			this.track_search_timer = setTimeout(_.bind(this.track_search, this), 2000);
		},

		track_search: function() {
			if(this.track_search_timer) {
				clearTimeout(this.track_search_timer);
				this.track_search_timer = null;
				mbp_track_event('help_search', {search: this.search});
				console.log(this.search);
			}
		},
	});

	mybookprogress.HelpTopicView = View.extend({
		template: mybookprogress.utils.template(jQuery('#help_topic_template').html()),

		events: {
			'click .mbp-help-search-link': 'search_clicked',
			'click .mbp-help-topic-link': 'topic_clicked',
			'click .mbp-help-topic-feedback-yes': 'feedback_helpful',
			'click .mbp-help-topic-feedback-no': 'feedback_unhelpful',
		},

		initialize: function(options) {
			this.topic = options.topic;
			this.ref_data = options.ref_data;
		},

		render: function(options) {
			View.prototype.render.call(this);
			this.render_breadcrumbs();
		},

		search_clicked: function(e) {
			mybookprogress.help.show_search(jQuery(e.target).attr('data-mbp-help-search'));
		},

		topic_clicked: function(e) {
			mybookprogress.help.show_topic(jQuery(e.target).attr('data-mbp-help-topic'), {type:'topic', topic: this.topic.slug, ref:this.ref_data});
		},

		render_breadcrumbs: function() {
			this.$('.mbp-help-topic-breadcrumbs').empty();
			var ref = this.ref_data;
			while(ref) {
				if(ref.type === 'search') {
					var search = ref.search.length > 8 ? ref.search.substring(0, 8)+'...' : ref.search;
					var link = jQuery('<span class="mbp-help-search-link"></span>');
					link.attr('data-mbp-help-search', ref.search);
					link.text('Search: "'+search+'"');
					var new_breadcrumb = jQuery('<span class="mbp-breadcrumb"></span>').append(link);
					this.$('.mbp-help-topic-breadcrumbs').prepend(jQuery('<span class="mbp-separator">&raquo;</span>'));
					this.$('.mbp-help-topic-breadcrumbs').prepend(new_breadcrumb);
				} else if(ref.type === 'topic') {
					var topic = mybookprogress.help_topics.get(ref.topic);
					var new_breadcrumb = jQuery('<span class="mbp-breadcrumb"><span class="mbp-help-topic-link" data-mbp-help-topic="'+ref.topic+'">'+topic.short_title+'</span></span>');
					this.$('.mbp-help-topic-breadcrumbs').prepend(jQuery('<span class="mbp-separator">&raquo;</span>'));
					this.$('.mbp-help-topic-breadcrumbs').prepend(new_breadcrumb);
				}
				ref = ref.ref;
			}
			if(!this.$('.mbp-help-topic-breadcrumbs').find('.mbp-help-search-link').length) {
				this.$('.mbp-help-topic-breadcrumbs').prepend(jQuery('<span class="mbp-separator">&raquo;</span>'));
				this.$('.mbp-help-topic-breadcrumbs').prepend(jQuery('<span class="mbp-breadcrumb"><span class="mbp-help-search-link" data-mbp-help-search="">'+mybookprogress_i18n.search+'</span></span>'));
			}
			this.$('.mbp-help-topic-breadcrumbs').append(jQuery('<span class="mbp-breadcrumb">'+this.topic.short_title+'</span>'));
		},

		feedback_helpful: function() {
			if(this.left_feedback) { return; }

			var track_helpful = _.bind(function() {
				mbp_track_event('help_feedback', {helpful: true, topic: this.topic.slug, ref:this.ref_data});
				this.show_feedback_message();
			}, this);

			this.confirm_enable_tracking(track_helpful);
		},

		feedback_unhelpful: function() {
			if(this.left_feedback) { return; }

			var track_unhelpful = _.bind(function() {
				mbp_track_event('help_feedback', {helpful: false, topic: this.topic.slug, ref:this.ref_data});
				this.show_feedback_message();
			}, this);

			this.confirm_enable_tracking(track_unhelpful);
		},

		confirm_enable_tracking: function(on_confirm) {
			if(settings.get('allow_tracking') !== 'yes') {
				var enable_tracking_modal = jQuery(mybookprogress.utils.template(jQuery('#help_enable_tracking_modal_template').html())());
				enable_tracking_modal.on('click', '.mbp-yes', _.bind(function() {
					settings.set('allow_tracking', 'yes');
					on_confirm();
					tb_remove();
				}, this));
				enable_tracking_modal.on('click', '.mbp-no', tb_remove);
				utils.modal(enable_tracking_modal);
			} else {
				on_confirm();
			}
		},

		show_feedback_message: function() {
			var message = mybookprogress_i18n.thank_you_for_feedback;
			var options = {
				position: {
					my: "right top+15",
					at: "right bottom",
					collision: "none",
					using: function(position, feedback) {
						jQuery(this).css(position);
						jQuery("<div>").addClass("arrow right top").appendTo(this);
					}
				}
			};
			var elements = this.$('.mbp-help-topic-feedback-yes, .mbp-help-topic-feedback-no');
			this.left_feedback = true;
			mybookprogress.utils.tooltip(elements, message, options);
		},
	});

	mybookprogress.help_topics = mybookprogress.help_topics || {};
	mybookprogress.help_topics.get = function(slug) {
		var topic = mybookprogress.help_topics[slug];
		if(topic) {
			topic = _.clone(topic);
			topic.slug = slug;
			return topic;
		}
	}

	mybookprogress.help_topics.book_setup = {
		common: true,
		short_title: 'Book Setup',
		title: 'How do I set up my book?',
		keywords: 'setup, edit, change, update, modify, book, title, phase, deadline, goal, target',
		content: ' ' +
'<ol> ' +
'<li>Click the "Setup" button next to the title of your book at the top of the Progress tab.</li> ' +
'<li>Enter the title of your book.</li> ' +
'<li>Select a template for your book <span class="mbp-help-topic-link" data-mbp-help-topic="phases">Phases</span>.</li> ' +
'<li>Click on each <span class="mbp-help-topic-link" data-mbp-help-topic="phases">Phase</span> to customize its deadline and goals. (optional)</li> ' +
'<li>Click the "Save Book" button at the bottom of the page.</li> ' +
'</ol> ' +
'',
	};
	mybookprogress.help_topics.phases = {
		short_title: 'Phases',
		title: 'What are book phases?',
		keywords: 'book, phase, sections',
		content: ' ' +
'<p> ' +
'Publishing a book requires more than just typing into Microsoft Word. Phases allow you to track and show progress across the entire publishing process, from outlining to proofreading. ' +
'</p> ' +
'<p> ' +
'Different kinds of books track progress in different ways. We have created some templates to get you started. You can customize any of these templates, by adding, removing, and modifying the phases. ' +
'</p> ' +
'<p> ' +
'You can track your progress within each phase by chapters, pages, words, scenes, or with a percentage slider. ' +
'</p> ' +
'',
	};
	mybookprogress.help_topics.add_books = {
		short_title: 'Add Books',
		title: 'How do I add a second book?',
		keywords: 'add, new, additional, create, make, book',
		content: ' ' +
'<p> ' +
'<em>To add multiple books you will need to <span class="mbp-help-topic-link" data-mbp-help-topic="upgrades">upgrade</span> to the professional version of MyBookProgress.</em> ' +
'</p> ' +
'<p> ' +
'To add an additional book, select the "Add New Book" option from the "Books" drop down menu at the top of the Progress tab on your MyBookProgress page. ' +
'This will take you to the <span class="mbp-help-topic-link" data-mbp-help-topic="book_setup">Book Setup</span> page which will allow you to customize its title, phases, etc. ' +
'</p> ' +
'',
	};
	mybookprogress.help_topics.update_progress = {
		short_title: 'Updating Progress',
		title: 'How do I update my progress?',
		keywords: 'add, new, additional, create, make, update, progress',
		content: ' ' +
'<ol> ' +
'<li>On the Progress tab, scroll down to the section that says "Update Progress".</li> ' +
'<li>Enter your total progress that you have made so far. So if you had already written 2000 words, and wrote 1500 more words today, you will want to type "3500" into the current progress field.</li> ' +
'<li>Click "Save progress"</li> ' +
'</ol> ' +
'<p> ' +
'See also: <span class="mbp-help-topic-link" data-mbp-help-topic="edit_progress">Editing Progress</span> ' +
'</p> ' +
'',
	};
	mybookprogress.help_topics.edit_progress = {
		short_title: 'Edit Progress',
		title: 'How do I edit my progress?',
		keywords: 'edit, change, update, modify, progress, date, time, value',
		content: ' ' +
'<ol> ' +
'<li>Scroll down on the Progress Tab to the Progress Timeline under the Update Progress section.</li> ' +
'<li>Click on the progress entry you want to edit.</li> ' +
'<li>Click the "Edit" button.</li> ' +
'<li>To save your changes, click away from that entry. Once it collapses back down to its normal state, your changes have been saved.</li> ' +
'</ol> ' +
'<p> ' +
'See also: <span class="mbp-help-topic-link" data-mbp-help-topic="share_progress">Sharing Progress</span> ' +
'</p> ' +
'',
	};
	mybookprogress.help_topics.share_progress = {
		short_title: 'Share Progress',
		title: 'How do I share my progress?',
		keywords: 'facebook, twitter, blog, share, social, media, progress',
		content: ' ' +
'<ol> ' +
'<li>Scroll down on the Progress Tab to the Progress Timeline under the Update Progress section.</li> ' +
'<li>Click on the progress entry you want to share.</li> ' +
'<li>Enter a message about your progress in the text box that appears.</li> ' +
'<li>Click on one of the social sharing buttons below the text box to share it to your blog, facebook, or twitter.</li> ' +
'</ol> ' +
'<p> ' +
'See also: <span class="mbp-help-topic-link" data-mbp-help-topic="edit_progress">Editing Progress</span> ' +
'</p> ' +
'',
	};
	mybookprogress.help_topics.style_packs = {
		short_title: 'Style Packs',
		title: 'What are Style Packs?',
		keywords: 'look, display, change, modify',
		content: ' ' +
'<p> ' +
'Style packs change the look and feel of your progress bar. MyBookTable comes with two style packs by default. If you are a savvy web developer, you can make your own style pack. ' +
'You can also get more style packs by upgrading to MyBookProgress Pro (coming soon). ' +
'</p> ' +
'',
	};
	mybookprogress.help_topics.mybooktable_integration = {
		short_title: 'MyBookTable Integration',
		title: 'What is MyBookTable Integration?',
		keywords: 'my, book, table, booktable, mybooktable, integration',
		content: ' ' +
'<p> ' +
'MyBookTable is the #1 Bookstore Plugin for WordPress. MyBookTable allows you to create a Google-friendly book page for your book with Pre-Order buttons, product description, and more. ' +
'</p> ' +
'<p> ' +
'With MyBookTable integration, you can connect your Progress Bar with your MyBookTable Book page. ' +
'</p> ' +
'',
	};
	mybookprogress.help_topics.linking_back = {
		short_title: 'Linking Back',
		title: 'Why should I link back to Author Media?',
		keywords: 'what, link, back, linkback, share, the, love',
		content: ' ' +
'<p> ' +
'This helps more people discover MyBookProgress and saves you from answering emails from people asking you what plugin you are using for your progress bar. ' +
'</p> ' +
'',
	};
	mybookprogress.help_topics.mailchimp_settings = {
		short_title: 'MailChimp Settings',
		title: 'How do I set up MailChimp?',
		keywords: 'mailchimp, mailing, list, setup',
		content: ' ' +
'<ol> ' +
'<li>Log into your MailChimp account and copy your MailChimp API Key. For steps on how to do this, see <a href="http://kb.mailchimp.com/accounts/management/about-api-keys" target="_blank">How to Find Your MailChimp API Key</a>.</li> ' +
'<li>Go to the Promote tab on your MyBookProgress page</li> ' +
'<li>Select the "MailChimp" option as your mailing list service.</li> ' +
'<li>Paste your MailChimp API Key from step 1 into the field labeled "Enter MailChimp API Key:"</li> ' +
'<li>Click the refresh button next to the MailChimp API Key field. If your key shows up as red, that means that it is invalid. Check to make sure you did not paste in an extra space at the beginning or end of the API Key.</li> ' +
'<li>Select the MailChimp list that you want your users to be subscribed to.</li> ' +
'</ol> ' +
'',
	};
	mybookprogress.help_topics.other_mailinglist_settings = {
		short_title: 'Other Mailinglist',
		title: 'How do I use the "Other" mailing list?',
		keywords: 'other, mailing, list, setup',
		content: ' ' +
'<p> ' +
'The "Other" option on the <span class="mbp-help-topic-link" data-mbp-help-topic="mailing_lists">Mailing List Settings</span> section of the Promote tab is used when you have a mailing list service that is not supported by MyBookProgress, ' +
'or you simply want to redirect users to a page instead of using a mailing list service. You can input any URL into the "list subscribe URL" field, and your users will be redirected there when they choose to recieve your updates. ' +
'</p> ' +
'',
	};
	mybookprogress.help_topics.mailing_lists = {
		common: true,
		short_title: 'Mailing Lists',
		title: 'How do I set up my mailing list?',
		keywords: 'mailing, list, service, provider, signup, setup, change',
		content: ' ' +
'<ol> ' +
'<li>Go to the Promote tab on your MyBookProgress page</li> ' +
'<li>Choose which mailing list service you are using. If you want to simply redirect users to a page instead of using a mailing list service, choose "Other".</li> ' +
'<li>Configure your mailing list service settings. See <span class="mbp-help-topic-link" data-mbp-help-topic="mailchimp_settings">"MailChimp" Settings</span> and ' +
'<span class="mbp-help-topic-link" data-mbp-help-topic="other_mailinglist_settings">"Other" Settings</span>.</li> ' +
'</ol> ' +
'',
	};
	mybookprogress.help_topics.widgets = {
		common: true,
		short_title: 'Widgets',
		title: 'What widget options are available?',
		keywords: 'widget, show, progress, display',
		content: ' ' +
'<p> ' +
'Go to the "Widgets" page, under "Appearance" on your WordPress admin menu. You should then see the "MyBookProgress" option in the "Available Widgets" section. ' +
'Drag it over to the appropriate widget section where you would like it to display (footer, sidebar, homepage, etc). ' +
'If there is a place on your website you want to add a widget and don’t see it as an option, talk to your theme developer about adding a widgetized zone or use a shortcode. ' +
'</p> ' +
'<p> ' +
'Once you have created the MyBookProgress widget, you can then customize its settings by clicking on it to expand the settings box. The MyBookProgress widget is capable of displaying the progress of all your books or only a single one. ' +
'</p> ' +
'',
	};
	mybookprogress.help_topics.shortcodes = {
		common: true,
		short_title: 'Shortcodes',
		title: 'What shortcode options are available?',
		keywords: 'shortcode, show, progress, display',
		content: ' ' +
'<p> ' +
'MyBookProgress has a simple shortcode wizard that helps you easily add MyBookTable shortcodes to your site. ' +
'Just click the "Insert Shortcode" button above the post content editor when editing a post or page and the wizard will appear and walk you through the shortcode options. ' +
'</p> ' +
'<p> ' +
'All of the MyBookProgress shortcodes use the [mybookprogress] core. Inserting [mybookprogress] by itself will show all of your books. ' +
'</p> ' +
'<p> ' +
'[mybookprogress]<br> ' +
'Show progress for all books ' +
'</p> ' +
'<p> ' +
'[mybookprogress book="1"] ' +
'Show book progress for book 1. The first book you add will be book 1, the second book will be book 2, and so on. ' +
'</p> ' +
'<p> ' +
'[mybookprogress book="1" showsubscribe="false"] ' +
'Show progress for book 1 but do not show the subscribe button. If you want to show the subscribe button, replace “false” with “true.” ' +
'</p> ' +
'<p> ' +
'[mybookprogress book="1" showsubscribe="true" simplesubscribe="false"] ' +
'Show progress for book 1. Show the subscribe button. Do not use the simple subscribe form. ' +
'</p> ' +
'<p> ' +
'[mybookprogress progress_id="21"] ' +
'Show progress at a specific point in time. The only way to use this shortcode is to either create it through the wizard or to click the "make blog post" button in the Progress tab of MyBookProgress. ' +
'This will create a snapshot of the current moment in time that won\'t change as you make more progress in the future. ' +
'</p> ' +
'',
	};
	mybookprogress.help_topics.upgrades = {
		common: true,
		short_title: 'Upgrades',
		title: 'What is an upgrade?',
		keywords: 'purchase, upgrade, nudges, email updates, better, faster, stronger',
		content: ' ' +
'<p> ' +
'Selling upgrades is the primary way we fund the development of MyBookProgress. By purchasing an upgrade you help make MyBookProgress even better. ' +
'</p> ' +
'<p> ' +
'To purchase an upgrade, go to <a href="http://www.mybookprogress.com" target="_blank">www.mybookprogress.com</a> and purchase an Upgrade License Key. ' +
'This License Key will allow you to download and use your MyBookProgress Upgrade plugin. You must enter this Key on the Upgrade tab of your MyBookProgress page to enable your Upgrade. ' +
'</p> ' +
'',
	};
})();
