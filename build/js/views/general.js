(function () {
	var utils = mybookprogress.utils;

	var View = mybookprogress.View = Backbone.View.extend({
		render: function() {
			this.$el.empty();
			if(this.template) { this.$el.html(this.template()); }
			this.render_subviews();
			this.render_bindings();
			return this;
		},

		render_subviews: function() {
			if(!this.subviews) { return; }
			_.each(this.subviews, this._render_subview, this);
		},

		render_subview: function(selector) {
			if(!this.subviews) { throw 'selector not found'; }
			var view = this.subviews[selector];
			if(!view) { throw 'selector not found'; }
			this._render_subview(view, selector);
			return this;
		},

		_render_subview: function(view, selector) {
			view.setElement(this.$(selector)).render();
		},

		set_subview: function(selector, view) {
			if(typeof selector !== 'string') { throw 'invalid selector'; }
			this.subviews = this.subviews || {};
			if(selector in this.subviews) { this.remove_subview(selector); }
			this.subviews[selector] = view;
			if(this.$(selector).length) { this._render_subview(view, selector); }
		},

		get_subview: function(selector) {
			return this.subviews[selector];
		},

		remove_subview: function(selector) {
			if(typeof this.subviews[selector] === 'undefined') { throw 'selector not found'; }
			this.subviews[selector].off();
			this.subviews[selector].stopListening();
			this.subviews[selector].undelegateEvents();
			delete this.subviews[selector];
			this.$(selector).empty();
		},

		render_bindings: function() {
			if(!this._binding_data) { return this; }
			for(var i = this._binding_data.length - 1; i >= 0; i--) {
				this._on_binding_change(this._binding_data[i].data, null, null, {});
			}
			return this;
		},

		_on_binding_input: function(binding_data, event) {
			this.model.set(binding_data.attribute, binding_data.from_input(jQuery(event.target).val(), this.model.get(binding_data.attribute)), {no_render: true});
		},

		_on_binding_change: function(binding_data, model, value, options) {
			if(options.no_render) { return; }
			var elements = binding_data.selector ? this.$(binding_data.selector) : this.$el;
			elements.val(binding_data.to_input(this.model.get(binding_data.attribute)));
		},

		delegateEvents: function(events) {
			Backbone.View.prototype.delegateEvents.call(this, events);
			var bindings = _.result(this, 'bindings');
			if(!bindings || !this.model) { return this; }
			var splitter_regex = /^(\S+)\s*(.*)$/;
			this._binding_data = [];
			for(var key in bindings) {
				var binding_data = {};

				var methods = bindings[key];
				if(!methods) {
					binding_data.from_input = _.identity;
					binding_data.to_input = _.identity;
				} else if(typeof methods === 'string') {
					var match = methods.match(splitter_regex);
					if(match) {
						binding_data.from_input = match[1];
						binding_data.to_input = match[2];
					}
				} else {
					binding_data.from_input = methods.from_input;
					binding_data.to_input = methods.to_input;
				}
				if(typeof binding_data.from_input === 'string') { binding_data.from_input = this[binding_data.from_input]; }
				if(typeof binding_data.to_input === 'string') { binding_data.to_input = this[binding_data.to_input]; }

				if(!_.isFunction(binding_data.to_input)) { binding_data.to_input = _.identity; }
				if(!_.isFunction(binding_data.from_input)) { throw 'invalid methods for binding'; }
				binding_data.to_input = _.bind(binding_data.to_input, this);
				binding_data.from_input = _.bind(binding_data.from_input, this);

				var match = key.match(splitter_regex);
				if(!match) { throw 'invalid attribute/selector for binding'; }

				binding_data.attribute = match[1];
				binding_data.selector = match[2];

				var on_input = _.bind(this._on_binding_input, this, binding_data);
				var input_event = 'input.delegateEvents' + this.cid;
				var change_event = 'change.delegateEvents' + this.cid;
				if(binding_data.selector === '') {
					this.$el.on(input_event, on_input);
					this.$el.on(change_event, on_input);
				} else {
					this.$el.on(input_event, binding_data.selector, on_input);
					this.$el.on(change_event, binding_data.selector, on_input);
				}

				var on_change = _.bind(this._on_binding_change, this, binding_data);
				var change_event = 'change:'+binding_data.attribute;
				this.listenTo(this.model, change_event, on_change);

				this._binding_data.push({data: binding_data, model: this.model, event: change_event, on_change: on_change});
			}
			return this;
		},

		undelegateEvents: function() {
			Backbone.View.prototype.undelegateEvents.call(this);
			if(!this._binding_data) { return this; }
			for(var i = this._binding_data.length - 1; i >= 0; i--) {
				var binding_data = this._binding_data[i];
				this.stopListening(binding_data.model, binding_data.event, binding_data.on_change);
			}
			this._binding_data = null;
			return this;
		},
	});

	mybookprogress.CollectionView = Backbone.View.extend({
		initialize: function(options) {
			options = options || {};
			if(typeof options.item_view !== 'undefined') { this.item_view = options.item_view; }
			this.sortable = this.sortable || options.sortable || false;

			if(!this.item_view) { throw 'no item view constructor provided'; }
			if(this.sortable && !('sorting_attr' in this.sortable)) {
				throw('sortable collection must have model sorting attribute provided');
			}

			if(this.sortable) {
				this.collection.sortBy(function(m) { return m.get(this.sortable['sorting_attr']); }, this);
			}

			this.item_views = [];
			this.collection.each(function(model, i) {
				var new_view = new this.item_view({model: model, parent: this});
				this.item_views.push(new_view);
				if(this.sortable) { model.set(this.sortable['sorting_attr'], i); }
			}, this);

			this.collection.on('add', this.add, this);
			this.collection.on('remove', this.remove, this);
			this.collection.on('sort', this.onsort, this);
			this.collection.on('reset', this.onreset, this);
		},

		add: function(model) {
			var new_view = new this.item_view({model: model});
			var index = null;

			if(this.sortable) {
				this.item_views.push(new_view);
				this.item_views = _.sortBy(this.item_views, function(view) { return view.model.get(this.sortable['sorting_attr']); }, this);
				_.each(this.item_views, function(view, i) { view.model.set(this.sortable['sorting_attr'], i); }, this);
				this.collection.sortBy(function(model) { return model.get(this.sortable['sorting_attr']); }, this);
				index = _.indexOf(this.item_views, new_view);
			} else {
				index = _.indexOf(this.collection.models, model);
				if(this.collection.models.length > this.item_views.length + 1) {
					if(index === this.collection.models.length-1) {
						index = this.item_views.length;
					} else {
						index = this.item_views.length;
						for(var i = 0; i < this.item_views.length; i++) {
							if(this.item_views[i].model === this.collection.models[index+1]) {
								index = i;
							}
						}
					}
				}
				this.item_views.splice(index, 0, new_view);
			}

			if(index == 0) {
				this.$el.prepend(new_view.render().el);
			} else {
				this.item_views[index-1].$el.after(new_view.render().el);
			}
		},

		remove: function(model) {
			var old_view = _(this.item_views).select(function(view) { return view.model === model; })[0];
			this.item_views = _(this.item_views).without(old_view);
			old_view.remove();

			if(this.sortable) {
				this.sortupdate();
			}
		},

		render: function() {
			this.$el.empty();

			_.each(this.item_views, function(view) {
				this.$el.append(view.setElement(view.el).render().el);
			}, this);

			if(this.sortable && !this.$el.data("ui-sortable")) {
				this.$el.sortable(this.sortable).on('sortupdate', _.bind(this.sortupdate, this));
			}

			return this;
		},

		onsort: function() {
			if(this.collection.comparator && _.isFunction(this.collection.comparator) && this.collection.comparator.length === 1) {
				this.item_views = _.sortBy(this.item_views, function(v) { return this.collection.comparator(v.model); }, this);
			}

			this.render();
		},

		onreset: function() {
			this.item_views = [];
			this.collection.each(function(model, i) {
				var new_view = new this.item_view({model: model});
				this.item_views.push(new_view);
				if(this.sortable) { model.set(this.sortable['sorting_attr'], i); }
			}, this);
			this.render();
		},

		sortupdate: function() {
			_.each(this.item_views, function(view) { view.model.set(this.sortable['sorting_attr'], view.$el.index()); }, this);
			this.item_views = _.sortBy(this.item_views, function(view) { return view.model.get(this.sortable['sorting_attr']); }, this);
			this.collection.models = _.sortBy(this.collection.models, function(model) { return model.get(this.sortable['sorting_attr']); }, this);
			this.collection.trigger('sort');
		},

		model_view: function(model) {
			for(var i = this.item_views.length - 1; i >= 0; i--) {
				if(this.item_views[i].model == model) { return this.item_views[i]; }
			}
			return null;
		},

		each: function(iteratee, context) {
			return _.each(this.item_views, iteratee, context);
		},
	});

	mybookprogress.TabsView = View.extend({
		add_tab: function(slug, name, view) {
			this.tabs = this.tabs || [];
			this.tabs.push({slug: slug, name: name, view: view});
		},

		remove_tab: function(slug) {
			for(var i = this.tabs.length - 1; i >= 0; i--) {
				if(this.tabs[i].slug == slug) {
					this.item_views.splice(i, 1);
					return;
				}
			}
		},

		get_tab: function(slug) {
			for(var i = this.tabs.length - 1; i >= 0; i--) {
				if(this.tabs[i].slug == slug) {
					return this.tabs[i].view;
				}
			}
		},

		render: function() {
			View.prototype.render.call(this);
			this.render_tabs();
			return this;
		},

		render_tabs: function() {
			if(!this.tabs) { return; }

			var root = this.get_root();
			var nav = this.get_nav();
			var content = this.get_content();

			_.each(this.tabs, function(tab) {
				nav.append(jQuery('<li><a href="#'+tab.slug+'">'+tab.name+'</a></li>'));
				content.append(tab.view.setElement(tab.view.el).render().$el.attr('id', tab.slug));
			}, this);

			root.tabs({activate: _.bind(this.on_tabsactivate, this)});
			this.tabs_initialized = true;
			if(this.activatetab) { this.focus_tab(this.activatetab); this.activatetab = null; }
		},

		on_tabsactivate: function(event, ui) {
			this.trigger('activatetab', ui.newPanel.attr('id'));
		},

		focus_tab: function(slug) {
			if(!this.tabs_initialized) { this.activatetab = slug; }
			for(var i = this.tabs.length - 1; i >= 0; i--) {
				if(this.tabs[i].slug == slug) {
					this.get_root().tabs({active: i});
				}
			}
		},

		get_root: function() {
			return this.tabs_root ? this.$(this.tabs_root) : this.$el;
		},

		get_nav: function() {
			return this.get_root().find(this.tabs_nav ? this.tabs_nav : 'ol, ul').eq(0);
		},

		get_content: function() {
			return this.get_root().find(this.tabs_content ? this.tabs_content : 'div').eq(0);
		},
	});

	mybookprogress.ModalView = View.extend({
		initialize: function(options) {
			this.open(options);
		},

		open: function(options) {
			utils.modal(this.render().$el, options);
		},

		close: function() {
			tb_remove();
		},
	});

	mybookprogress.HiddenView = View.extend({
		render: function() {
			this.$el.hide();
			return this;
		},
	});
})();
