(function () {
	mybookprogress.AdminView = mybookprogress.TabsView.extend({
		el: jQuery('#mbp-admin-page'),
		template: mybookprogress.utils.template(jQuery('#admin_template').html()),
		initialize: function() {
			this.tabs_root = '.mbp-admin-tabs';
			this.tabs_content = '.mbp-admin-tabs-content';
			this.add_tab('mbp-progress-tab', mybookprogress_i18n.progress, mybookprogress.progress_tab = new mybookprogress.ProgressTabView());
			this.add_tab('mbp-promote-tab', mybookprogress_i18n.promote, mybookprogress.promote_tab = new mybookprogress.PromoteTabView());
			this.add_tab('mbp-style-tab', mybookprogress_i18n.style, mybookprogress.style_tab = new mybookprogress.StyleTabView());
			this.add_tab('mbp-display-tab', mybookprogress_i18n.display, mybookprogress.display_tab = new mybookprogress.DisplayTabView());
			this.add_tab('mbp-upgrade-tab', mybookprogress_i18n.upgrade, mybookprogress.upgrade_tab = new mybookprogress.UpgradeTabView());
			this.set_subview('.mbp-help', mybookprogress.help = new mybookprogress.HelpView());
			this.set_subview('.mbp-sir-walter', mybookprogress.sir_walter = new mybookprogress.SirWalterView());
			this.render();

			if(mybookprogress.utils.get_query_var('tab')) { this.focus_tab(mybookprogress.utils.get_query_var('tab')); }
			this.on('activatetab', function(slug) { mbp_track_event('view_'+slug.replace(/-/g, '_')); });
		}
	});
})();
