(function () {
	/*---------------------------------------------------------*/
	/* Numeric Progress                                        */
	/*---------------------------------------------------------*/

	var numeric_progress_editor_template = function(data) {
		var element = jQuery(mybookprogress.utils.template(jQuery('#book_progress_type_numeric_editor_template').html())(data));
		element.on('input', '[name="progress"]', function(e) {
			var max = parseInt(element.find('[name="target"]').val());
			var val = parseInt(jQuery(e.target).val());
			if(isNaN(val)) { jQuery(e.target).val(1); }
			else if(val > max) { jQuery(e.target).val(max); }
			else if(val < 1) { jQuery(e.target).val(1); }
			else if(val.toFixed(0) !== jQuery(e.target).val()) { jQuery(e.target).val(val.toFixed(0)); }
		});
		return element;
	};
	var numeric_progress_display_template = function(data) {
		return jQuery(mybookprogress.utils.template(jQuery('#book_progress_type_numeric_display_template').html())(data));
	};
	var numeric_progress_save = function(form_data) {
		return {
			progress: parseInt(form_data.progress)/parseInt(form_data.target),
			target: parseInt(form_data.target),
		};
	};

	/*---------------------------------------------------------*/
	/* Percentage Progress                                     */
	/*---------------------------------------------------------*/

	var percent_progress_editor_template = function(data) {
		var element = jQuery(mybookprogress.utils.template(jQuery('#book_progress_type_percent_editor_template').html())(data));
		element.find('.mbp-slider').slider({animate: "fast", value:data.progress, min: 0, max: 1, step: 0.01, slide: function(event, ui) {
			element.find('[name="progress"]').val(Math.round(ui.value*100)).trigger('change');
		}});
		element.find('[name="progress"]').val(Math.round(data.progress*100)).on('input', function() {
			var val = jQuery(this).val();
			if(!val.match(/^[0-9]*$/)) { val = val.replace(/[^0-9]/g, ''); jQuery(this).val(val); }
			if(val == '') {
				val = 0;
			} else {
				val = parseInt(val)*0.01;
			}
			if(val < 0) { val = 0; jQuery(this).val(Math.round(val*100)); }
			if(val > 1) { val = 1; jQuery(this).val(Math.round(val*100)); }

			element.find('.mbp-slider').slider('value', val);
		});
		return element;
	};
	var percent_progress_display_template = function(data) {
		return jQuery(mybookprogress.utils.template(jQuery('#book_progress_type_percent_display_template').html())(data));
	};
	var percent_progress_save = function(form_data) {
		return {
			progress: form_data.progress == '' ? 0 : parseInt(form_data.progress)*0.01,
			target: 100,
		};
	};

	mybookprogress.progress_types = mybookprogress.progress_types || {};
	mybookprogress.default_progress_type = 'words';
	mybookprogress.progress_types.chapters = {name: mybookprogress_i18n.chapters_name, unit: mybookprogress_i18n.chapters_unit, units: mybookprogress_i18n.chapters_units, default_target: 15, editor: numeric_progress_editor_template, display: numeric_progress_display_template, save: numeric_progress_save};
	mybookprogress.progress_types.pages = {name: mybookprogress_i18n.pages_name, unit: mybookprogress_i18n.pages_unit, units: mybookprogress_i18n.pages_units, default_target: 200, editor: numeric_progress_editor_template, display: numeric_progress_display_template, save: numeric_progress_save};
	mybookprogress.progress_types.words = {name: mybookprogress_i18n.words_name, unit: mybookprogress_i18n.words_unit, units: mybookprogress_i18n.words_units, default_target: 50000, editor: numeric_progress_editor_template, display: numeric_progress_display_template, save: numeric_progress_save};
	mybookprogress.progress_types.scenes = {name: mybookprogress_i18n.scenes_name, unit: mybookprogress_i18n.scenes_unit, units: mybookprogress_i18n.scenes_units, default_target: 100, editor: numeric_progress_editor_template, display: numeric_progress_display_template, save: numeric_progress_save};
	mybookprogress.progress_types.percent = {name: mybookprogress_i18n.percent_name, unit: mybookprogress_i18n.percent_unit, units: mybookprogress_i18n.percent_units, hide_target_editor: true, default_target: 100, editor: percent_progress_editor_template, display: percent_progress_display_template, save: percent_progress_save};
})();
