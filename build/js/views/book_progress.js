(function () {
	var utils = mybookprogress.utils;
	var settings = null;
	var books = null;
	var progress_tab = null;

	mybookprogress.on('models_loaded', function() {
		settings = mybookprogress.settings;
		books = mybookprogress.books;
	});
	mybookprogress.on('loaded', function() {
		progress_tab = mybookprogress.progress_tab;
	});

	var View = mybookprogress.View;
	var CollectionView = mybookprogress.CollectionView;
	var TabsView = mybookprogress.TabsView;
	var VirtualModel = mybookprogress.VirtualModel;
	var VirtualCollection = mybookprogress.VirtualCollection;

	/*---------------------------------------------------------*/
	/* Book Progress View                                      */
	/*---------------------------------------------------------*/

	mybookprogress.BookProgressView = View.extend({
		template: mybookprogress.utils.template(jQuery('#book_progress_template').html()),

		initialize: function() {
			this.set_subview('.mbp-progress-view-container', new mybookprogress.BookProgressViewView({model: this.model}));
			this.set_subview('.mbp-book-tabs', new mybookprogress.BookProgressTabsView({parent: this, model: this.model}));
			this.set_subview('.mbp-create-progress-container', new mybookprogress.BookProgressCreateView({parent: this, model: this.model}));
			this.set_subview('.mbp-progress-timeline', new mybookprogress.BookProgressTimelineView({model: this.model}));
		},
	});

	/*---------------------------------------------------------*/
	/* Book Progress View Selector                             */
	/*---------------------------------------------------------*/

	mybookprogress.BookProgressViewView = View.extend({
		template: mybookprogress.utils.template(jQuery('#book_progress_view_template').html()),

		events: {
			'change .mbp-progress-view': 'progress_view_change',
			'click .mbp-setup-book-button': 'open_setup',
		},

		render: function() {
			View.prototype.render.call(this);
			this.render_options();
			return this;
		},

		render_options: function() {
			var select = this.$('.mbp-progress-view');
			select.empty();
			select.append(jQuery('<option value="overview">All Books</option>'));
			books.each(function(book) {
				select.append(jQuery('<option value="'+book.id+'"'+(progress_tab.currentbook && progress_tab.currentbook.id == book.id ? ' selected="selected"' : '')+'>'+book.get_title()+'</option>'));
			}, this);
			select.append(jQuery('<option value="new-book">-- Add New Book --</option>'));

			if(!progress_tab.currentbook) { this.$('.mbp-setup-book-button').addClass('mbp-disabled'); } else { this.$('.mbp-setup-book-button').removeClass('mbp-disabled'); }
		},

		progress_view_change: function(e) {
			book_id = jQuery(e.target).val();
			if(book_id == 'overview') {
				mbp_track_event('view_books_overview');
				progress_tab.show_overview();
			} else if(book_id == 'new-book') {
				progress_tab.show_book();
			} else {
				progress_tab.show_book(book_id);
			}
		},

		open_setup: function() {
			progress_tab.toggle_setup();
		},
	});

	/*---------------------------------------------------------*/
	/* Book Phase Display                                      */
	/*---------------------------------------------------------*/

	mybookprogress.BookPhasesItemView = View.extend({
		className: 'mbp-book-phase',
		template: mybookprogress.utils.template(jQuery('#book_phase_item_template').html()),

		events: {
			'click': 'select',
		},

		initialize: function(options) {
			this.parent = options.parent;
		},

		render: function() {
			View.prototype.render.call(this);
			var status = this.parent.model.get_phase_status(this.model.id);
			if(status === 'complete') {
				this.$el.addClass('mbp-phase-done');
			} else if(status === 'working') {
				this.$el.addClass('mbp-phase-working');
			}
			return this;
		},

		select: function() {
			this.model.trigger('select', this);
		},
	});

	mybookprogress.BookPhasesView = CollectionView.extend({
		className: 'mbp-book-phases',
		item_view: mybookprogress.BookPhasesItemView,
		initialize: function(options) {
			this.collection = new VirtualCollection(this.model.get_phases());
			CollectionView.prototype.initialize.call(this, options);
			this.listenTo(this.model, 'change:phases_status', this.render);
		},

		render: function() {
			CollectionView.prototype.render.call(this);
			if(this.collection.length > 4) { this.$el.addClass('mbp-small-phases'); } else { this.$el.removeClass('mbp-small-phases'); }
		},
	});

	/*---------------------------------------------------------*/
	/* Statistics Tab                                          */
	/*---------------------------------------------------------*/

	mybookprogress.BookProgressStatisticsTabView = View.extend({
		className: 'mbp-book-tab',
		template: mybookprogress.utils.template(jQuery('#book_progress_statistics_tab_template').html()),

		initialize: function() {
			if(this.model) {
				this.show_book_stats();
			} else {
				this.show_global_stats();
			}
		},

		fetch_stats: function() {
			if(this.fetching_stats == this.current_view) { return; }
			var fetching_stats = this.fetching_stats = this.current_view;

			var action = this.current_view == 'global' ? 'mbp_get_global_stats' : (this.current_view == 'book' ? 'mbp_get_book_stats' : 'mbp_get_phase_stats');
			var data = this.current_view == 'global' ? {} : (this.current_view == 'book' ? {book_id: this.model.id} : {book_id: this.model.id, phase_id: this.phase_id})

			mybookprogress.WPQuery(action, data).then(_.bind(function(response) {
				if(fetching_stats != this.current_view) { return; }
				if(response === null || typeof response !== 'object' || 'error' in response) { response = null; }

				this.fetching_stats = false;
				this.stats = response;
				this.render();
			}, this));
		},

		show_book_stats: function() {
			if(this.current_view == 'book') { return; }
			this.current_view = 'book';
			if(!this.listening_to_book) {
				this.listenTo(this.model, 'sync', this.fetch_stats);
				this.listenTo(this.model.get_progress(), 'sync', this.fetch_stats);
				this.listening_to_book = true;
			}
			this.fetch_stats();
		},

		show_global_stats: function() {
			if(this.current_view == 'global') { return; }
			if(this.listening_to_book) {
				this.stopListening(this.model);
				this.stopListening(this.model.get_progress());
				this.listening_to_book = false;
			}
			this.current_view = 'global';
			this.fetch_stats();
		},

		show_phase_stats: function(phase_id) {
			if(this.current_view == 'phase_'+phase_id) { return; }
			if(!this.listening_to_book) {
				this.listenTo(this.model, 'sync', this.fetch_stats);
				this.listenTo(this.model.get_progress(), 'sync', this.fetch_stats);
				this.listening_to_book = true;
			}
			this.current_view = 'phase_'+phase_id;
			this.phase_id = phase_id;
			this.fetch_stats();
		},

		render: function() {
			View.prototype.render.call(this);

			var stats = this.stats;
			if(!stats) {
				this.$('.mbp-stats-tab').addClass('mbp-no-data');
				stats = {
					progress_type: 'percent',
					progress_target: 100,
					current_per_day: 2.2,
					current_per_week: 15,
					current_per_month: 54,
					needed_per_day: 2,
					needed_per_week: 14,
					needed_per_month: 50,
					graph_data: [
						[utils.unix_timestamp_now(), 0.10],
						[utils.unix_timestamp_now()+86400*2.8, 0.64],
						[utils.unix_timestamp_now()+86400*3.5, 0.50],
						[utils.unix_timestamp_now()+86400*5, 0.90],
					],
					phases: {'Phase': 1},
				};
			} else {
				this.$('.mbp-stats-tab').removeClass('mbp-no-data');
			}

			if(stats.current_per_day) {
				this.$('.mbp-other-statistics-table').hide();
				this.$('.mbp-pace-statistics-table').removeAttr('style');

				var progress_units = mybookprogress_i18n.progress;
				if(mybookprogress.progress_types[stats.progress_type]) {
					var progress_units = mybookprogress.progress_types[stats.progress_type].units;
				}

				this.$('.mbp-unit-per-day .mbp-title-col').text(progress_units+'/'+mybookprogress_i18n.day);
				this.$('.mbp-unit-per-week .mbp-title-col').text(progress_units+'/'+mybookprogress_i18n.week);
				this.$('.mbp-unit-per-month .mbp-title-col').text(progress_units+'/'+mybookprogress_i18n.month);

				this.$('.mbp-unit-per-day .mbp-current-col').text(utils.progress_format(stats.current_per_day));
				this.$('.mbp-unit-per-week .mbp-current-col').text(utils.progress_format(stats.current_per_week));
				this.$('.mbp-unit-per-month .mbp-current-col').text(utils.progress_format(stats.current_per_month));
				if(stats.needed_per_day) {
					this.$('.mbp-needed-col').show();
					this.$('.mbp-unit-per-day .mbp-needed-col').text(utils.progress_format(stats.needed_per_day));
					this.$('.mbp-unit-per-week .mbp-needed-col').text(utils.progress_format(stats.needed_per_week));
					this.$('.mbp-unit-per-month .mbp-needed-col').text(utils.progress_format(stats.needed_per_month));
					if(stats.current_per_day > stats.needed_per_day) { this.$('.mbp-unit-per-day .mbp-current-col').addClass('mbp-on-pace'); } else { this.$('.mbp-unit-per-day .mbp-current-col').removeClass('mbp-on-pace'); }
					if(stats.current_per_week > stats.needed_per_week) { this.$('.mbp-unit-per-week .mbp-current-col').addClass('mbp-on-pace'); } else { this.$('.mbp-unit-per-week .mbp-current-col').removeClass('mbp-on-pace'); }
					if(stats.current_per_month > stats.needed_per_month) { this.$('.mbp-unit-per-month .mbp-current-col').addClass('mbp-on-pace'); } else { this.$('.mbp-unit-per-month .mbp-current-col').removeClass('mbp-on-pace'); }
				} else {
					this.$('.mbp-needed-col').hide();
				}
			} else {
				this.$('.mbp-other-statistics-table').removeAttr("style");
				this.$('.mbp-pace-statistics-table').hide();

				this.$('.mbp-most-productive-day .mbp-value-col').text(stats.most_productive_day);
			}

			var graph_data = jQuery.extend(true, [], stats.graph_data);
			if(graph_data.length) {
				if(stats.books) {
					var min = -1;
					var max = -1;
					for(var i = graph_data.length - 1; i >= 0; i--) {
						if(max == -1 || graph_data[i][0] > max) { max = graph_data[i][0]; }
						if(min == -1 || graph_data[i][0] < min) { min = graph_data[i][0]; }
						graph_data[i][0] = new Date(graph_data[i][0]*1000);
					}
					var duration = max-min;

					var width = this.$('.mbp-statistics-graph').width();
					var height = this.$('.mbp-statistics-graph').height();

					var options = {
						width: width,
						height: height,
						backgroundColor: 'transparent',
						chartArea: {'top': 5, 'left': 45, 'width': width-135, 'height': height-35},
						vAxis: { format:'#%', viewWindow: { min: 0, max: 1 } },
						hAxis: { viewWindow: { min: new Date((min-0.1*duration)*1000), max: new Date((max+0.1*duration)*1000) } },
						legend: {'position': 'right'},
						enableInteractivity: false,
					};

					var data = new google.visualization.DataTable();
					data.addColumn('datetime', mybookprogress_i18n.time);
					for(book in stats.books) {
						data.addColumn('number', stats.books[book]);
					}
					data.addRows(graph_data);

					var chart = new google.visualization.LineChart(this.$('.mbp-statistics-graph')[0]);
					chart.draw(data, options);
				} else if(stats.phases) {
					var min = -1;
					var max = -1;
					for(var i = graph_data.length - 1; i >= 0; i--) {
						if(max == -1 || graph_data[i][0] > max) { max = graph_data[i][0]; }
						if(min == -1 || graph_data[i][0] < min) { min = graph_data[i][0]; }
						graph_data[i][0] = new Date(graph_data[i][0]*1000);
					}
					var duration = max-min;

					var width = this.$('.mbp-statistics-graph').width();
					var height = this.$('.mbp-statistics-graph').height();

					var options = {
						width: width,
						height: height,
						backgroundColor: 'transparent',
						chartArea: {'top': 5, 'left': 35, 'width': width-35, 'height': height-35},
						vAxis: { format:'#%', viewWindow: { min: 0, max: 1 } },
						hAxis: { viewWindow: { min: new Date((min-0.1*duration)*1000), max: new Date((max+0.1*duration)*1000) } },
						legend: {'position': 'none'},
						enableInteractivity: false,
						lineWidth: 4,
						pointSize: 6,
					};

					var data = new google.visualization.DataTable();
					data.addColumn('datetime', mybookprogress_i18n.time);
					for(phase in stats.phases) {
						data.addColumn('number', stats.phases[phase]);
					}
					data.addRows(graph_data);

					var chart = new google.visualization.AreaChart(this.$('.mbp-statistics-graph')[0]);
					chart.draw(data, options);
				} else {
					var start = graph_data[0][0];
					var end = graph_data[graph_data.length-1][0];
					if(stats.deadline && stats.deadline > end) { end = stats.deadline; }
					var duration = end-start;
					var min_bound = new Date((start-0.1*duration)*1000);
					var middle = new Date((start+0.5*duration)*1000);
					var max_bound = new Date((end+0.1*duration)*1000);

					for(var i = graph_data.length - 1; i >= 0; i--) {
						graph_data[i] = graph_data[i];
						graph_data[i][0] = new Date(graph_data[i][0]*1000);
						graph_data[i].splice(1, 0, null);
						graph_data[i].push(null);
						graph_data[i].push(null);
					}

					var width = this.$('.mbp-statistics-graph').width();
					var height = this.$('.mbp-statistics-graph').height();

					var options = {
						width: width,
						height: height,
						backgroundColor: 'transparent',
						chartArea: {'top': 5, 'left': 50, 'width': width-50, 'height': height-35},
						legend: {'position': 'none'},
						series: {
							0: {color: '#CB3301', lineWidth: 4, pointSize: 6},
							1: {color: '#B5B5B5', lineWidth: 1.5, areaOpacity: 0},
						},
						vAxis: { viewWindow: { min: 0, max: stats.progress_target ? stats.progress_target*1.2 : 100 } },
						hAxis: { viewWindow: { min: min_bound, max: max_bound } },
						annotation: { 1 : { style: 'line' }, 4: { style: 'point' }, },
						annotations: { datum: { stemColor: 'transparent', stemLength: 5, textStyle: {color: 'black'} } },
						enableInteractivity: false,
					};

					if(stats.deadline) {
						graph_data.push([new Date(stats.deadline*1000), mybookprogress_i18n.deadline, null, null, null]);
					}

					if(stats.progress_target) {
						graph_data.push([min_bound, null, null, stats.progress_target, null]);
						graph_data.push([middle, null, null, stats.progress_target, 'Target']);
						graph_data.push([max_bound, null, null, stats.progress_target, null]);
					}

					var data = new google.visualization.DataTable();
					data.addColumn('datetime', mybookprogress_i18n.time);
					data.addColumn({type: 'string', role: 'annotation'});
					data.addColumn('number', mybookprogress_i18n.progress);
					data.addColumn('number', mybookprogress_i18n.target);
					data.addColumn({type: 'string', role: 'annotation'});
					data.addRows(graph_data);

					var chart = new google.visualization.AreaChart(this.$('.mbp-statistics-graph')[0]);
					chart.draw(data, options);
				}
			}

			return this;
		},
	});

	/*---------------------------------------------------------*/
	/* Nudges Tab                                              */
	/*---------------------------------------------------------*/

	mybookprogress.NudgeItemView = View.extend({
		className: 'mbp-nudge',
		template: mybookprogress.utils.template(jQuery('#book_progress_nudge_item_template').html()),

		events: {
			'mouseover': 'mouseover',
			'mouseout': 'mouseout',
			'click': 'clicked',
			'click .mbp-nudge-delete': 'delete',
		},

		render: function() {
			this.$el.html(this.template());
			if(!this.model.get('viewed')) { this.$el.addClass('mbp-nudge-new'); } else { this.$el.removeClass('mbp-nudge-new'); }
			return this;
		},

		delete: function() {
			this.model.destroy();
		},

		mouseover: function() {
			this.hover_timer = setTimeout(_.bind(this.set_viewed, this), 2000);
		},

		mouseout: function() {
			if(this.hover_timer) { clearTimeout(this.hover_timer); this.hover_timer = null; }
		},

		clicked: function() {
			this.set_viewed();
		},

		set_viewed: function() {
			if(this.model.get('viewed')) { return; }
			if(this.hover_timer) { clearTimeout(this.hover_timer); this.hover_timer = null; }
			this.model.set_viewed();
			this.render();
		},

		format_date: function() {
			return utils.format_date(this.model.get('timestamp'));
		},

		format_name: function() {
			var name = this.model.get('name');
			if(!name) { name = mybookprogress_i18n.anonymous; }
			if(this.model.get('email') && ['thomas@authormedia.com', 'tim@authormedia.com', 'support@authormedia.com'].indexOf(this.model.get('email')) == -1) {
				name = '<a href="mailto:'+this.model.get('email')+'?subject=Thank you for the nudge!">'+name+'</a>';
			}
			if(this.model.get('avatar')) { name = '<div class="mbp-nudge-author-image"><img src="'+this.model.get('avatar')+'"></div>'+name; }
			return name;
		},
	});

	mybookprogress.BookProgressNudgesTabView = View.extend({
		className: 'mbp-book-tab',
		template: mybookprogress.utils.template(jQuery('#book_progress_nudges_tab_template').html()),

		events: {
			'click .mbp-upsell-button': 'clicked_upsell_button',
		},

		initialize: function() {
			this.nudges = new VirtualCollection([
				{
					timestamp: utils.unix_timestamp_now(),
					book_id: 0,
					text: 'I come to your website every day to see your progress. Keep up the great work!',
					name: 'Reina Simmons',
					viewed: true,
				},
				{
					timestamp: utils.unix_timestamp_now(),
					book_id: 0,
					text: 'I can\'t wait for your next book. I hope you finish it soon!',
					name: 'Daniel White',
					viewed: true,
				},
				{
					timestamp: utils.unix_timestamp_now(),
					book_id: 0,
					text: 'I just noticed that you\'ve started on a new book, I\'m totally hyped!',
					name: 'Shelly Watson',
					viewed: true,
				},
			]);
			this.set_subview('.mbp-nudges-container', new CollectionView({item_view: mybookprogress.NudgeItemView, collection: this.nudges}));
		},

		render: function() {
			View.prototype.render.call(this);
			this.$el.addClass('mbp-disabled');
			var container = this.$('.mbp-nudges-container');
			container.scroll(_.bind(function() {
				var height = container.prop('scrollHeight') - container.height();
				if(height - container.scrollTop() < 50) {
					this.nudges.next_page();
				}
			}, this));
			return this;
		},

		clicked_upsell_button: function() {
			jQuery('.mbp-admin-tabs > .ui-tabs-nav a[href="#mbp-upgrade-tab"]').click();
		},
	});

	/*---------------------------------------------------------*/
	/* Email Updates Tab                                       */
	/*---------------------------------------------------------*/

	mybookprogress.EmailUpdatesTabView = View.extend({
		className: 'mbp-book-tab',
		template: mybookprogress.utils.template(jQuery('#book_progress_email_updates_tab_template').html()),

		events: {
			'input .mbp-email-updates-email': 'email_changed',
			'click .mbp-email-updates-test-email-button': 'send_test_email',
			'click .mbp-email-updates-period-button': 'update_period',
			'click .mbp-upsell-button': 'clicked_upsell_button',
		},

		initialize: function() {
			this.default_period = 'weekly';
			this.period_types = {
				daily: { name: mybookprogress_i18n.daily },
				weekly: { name: mybookprogress_i18n.weekly },
				monthly: { name: mybookprogress_i18n.monthly },
				never: { name: mybookprogress_i18n.never },
			};
			this.listenTo(this.model, 'change:email_updates_period', this.render_periods);
			this.editing_timer = null;
			this.email_status = null;
		},

		render: function() {
			this.$el.html(this.template());
			this.$el.addClass('mbp-disabled');
			this.render_email();
			this.render_periods();
			return this;
		},

		render_email: function() {
			var email = this.model.get('email_updates_email');
			if(!email) {
				email = this.$('.mbp-email-updates-email').attr('data-default');
				this.$('.mbp-email-updates-email').val(email);
				this.update_email();
			} else {
				this.$('.mbp-email-updates-email').val(email);
				if(this.verify_email()) {
					this.email_status = 'good';
				} else {
					this.email_status = 'bad';
				}
				this.render_feedback();
			}
		},

		render_feedback: function() {
			this.$('.mbp-email-updates-email').attr('class', 'mbp-email-updates-email');
			this.$('.mbp-email-updates-email').removeAttr('disabled');
			this.$('.mbp-email-updates-email-setting .mbp-setting-feedback').attr('class', 'mbp-setting-feedback');
			if(this.email_status == 'editing') {
				this.$('.mbp-email-updates-email-setting .mbp-setting-feedback').addClass('checking');
			} else if(this.email_status == 'good') {
				this.$('.mbp-email-updates-email').addClass('mbp-correct');
				this.$('.mbp-email-updates-email-setting .mbp-setting-feedback').addClass('good');
			} else if(this.email_status == 'bad') {
				this.$('.mbp-email-updates-email').addClass('mbp-error');
				this.$('.mbp-email-updates-email-setting .mbp-setting-feedback').addClass('bad');
			}
		},

		email_changed: function() {
			this.email_status = 'editing';
			if(this.editing_timer) { clearInterval(this.editing_timer); }
			this.editing_timer = setTimeout(_.bind(this.update_email, this), 1000);
			this.render_feedback();
		},

		update_email: function() {
			if(this.editing_timer) { clearInterval(this.editing_timer); this.editing_timer = null; }
			if(this.verify_email()) {
				this.email_status = 'editing';
				this.model.set('email_updates_email', this.$('.mbp-email-updates-email').val());
				this.model.save(null, {success: _.bind(this.email_updated, this)});
			} else {
				this.email_status = 'bad';
			}
			this.render_feedback();
		},

		email_updated: function() {
			if(!this.editing_timer) {
				this.email_status = 'good';
				this.render_feedback();
			}
		},

		verify_email: function() {
			return this.$('.mbp-email-updates-email').val().match(/^[^@]+@[^@]+$/);
		},

		send_test_email: function() {
			var button = this.$('.mbp-email-updates-test-email-button');
			if(button.hasClass('mbp-disabled')) { return; }
			button.addClass('mbp-disabled');
			mybookprogress.WPQuery('mbp_email_updates_send_test', {email: this.model.get('email_updates_email'), book: this.model.id, period: this.model.get('email_updates_period')}).then(function(response) {
				button.removeClass('mbp-disabled');
			});
		},

		render_periods: function() {
			var current_period = this.model.get('email_updates_period');
			if(!current_period) {
				current_period = this.default_period;
				this.model.set('email_updates_period', current_period);
				this.model.save();
			}
			this.$('.mbp-email-updates-periods').empty();
			for(type in this.period_types) {
				var new_button = jQuery('<div class="mbp-setting-button mbp-email-updates-period-button"></div>');
				new_button.attr('data-mbp-email-updates-period', type);
				new_button.text(this.period_types[type].name);
				if(type === current_period) { new_button.addClass('mbp-selected'); }
				this.$('.mbp-email-updates-periods').append(new_button);
			}
		},

		update_period: function(e) {
			this.model.set('email_updates_period', jQuery(e.target).attr('data-mbp-email-updates-period'));
			this.model.save();
		},

		clicked_upsell_button: function() {
			jQuery('.mbp-admin-tabs > .ui-tabs-nav a[href="#mbp-upgrade-tab"]').click();
		},
	});

	/*---------------------------------------------------------*/
	/* Book Tabs                                               */
	/*---------------------------------------------------------*/

	mybookprogress.BookProgressTabsView = TabsView.extend({
		template: mybookprogress.utils.template(jQuery('#book_progress_tabs_template').html()),

		initialize: function(options) {
			this.add_tab('mbp-stats-tab', this.model ? mybookprogress_i18n.pace : mybookprogress_i18n.stats, options.parent.stats_tab = new mybookprogress.BookProgressStatisticsTabView({model: this.model}));
			this.add_tab('mbp-nudges-tab', mybookprogress_i18n.nudges, options.parent.nudges_tab = new mybookprogress.BookProgressNudgesTabView({model: this.model}));
			if(this.model) {
				this.add_tab('mbp-email-updates-tab', mybookprogress_i18n.email_updates, options.parent.email_updates_tab = new mybookprogress.EmailUpdatesTabView({model: this.model}));
				if(window.location.hash.substr(1) == 'email-updates-settings') { this.focus_tab('mbp-email-updates-tab'); }
			}

			this.on('activatetab', function(slug) { mbp_track_event('view_'+slug.replace(/-/g, '_')); });
		},
	});

	/*---------------------------------------------------------*/
	/* Book Progress                                           */
	/*---------------------------------------------------------*/

	mybookprogress.BookProgressDisplay = View.extend({
		render: function() {
			var progress_type = this.get_progress_type();
			if(!progress_type) { return; }
			this.$el.empty().append(progress_type.display(this.get_progress_data()));
			return this;
		},

		get_progress_type: function() {
			return mybookprogress.progress_types[this.model.get('progress_type')];
		},

		get_progress_data: function() {
			return {utils: utils, progress_type: this.get_progress_type(), progress: this.model.get('progress'), target: this.model.get('target')};
		},
	});

	mybookprogress.BookProgressEditor = View.extend({
		events: {
			'input input': 'schedule_save',
			'change input': 'schedule_save',
		},

		render: function() {
			var progress_type = this.get_progress_type();
			if(!progress_type) { return; }
			this.$el.empty().append(progress_type.editor(this.get_progress_data()));
			return this;
		},

		schedule_save: function() {
			if(this.save_timer) { clearInterval(this.save_timer); }
			this.save_timer = setTimeout(_.bind(this.save, this), 10);
		},

		save: function() {
			if(this.save_timer) { clearInterval(this.save_timer); this.save_timer = null; }

			var progress_type = this.get_progress_type();
			var form_data = {};
			var form_inputs = _.map(this.$('input, textarea, select'), jQuery);
			for (var i = form_inputs.length - 1; i >= 0; i--) {
				form_data[form_inputs[i].attr('name')] = form_inputs[i].val();
			};
			form_data.progress_type = progress_type;
			form_data.utils = utils;
			var progress_data = progress_type.save(form_data);
			this.model.set({progress: progress_data.progress, target: progress_data.target});
		},

		get_progress_type: function() {
			return mybookprogress.progress_types[this.model.get('progress_type')];
		},

		get_progress_data: function() {
			return {utils: utils, progress_type: this.get_progress_type(), progress: this.model.get('progress'), target: this.model.get('target')};
		},
	});

	/*---------------------------------------------------------*/
	/* Progress Creation                                       */
	/*---------------------------------------------------------*/

	mybookprogress.BookProgressCreateView = View.extend({
		template: mybookprogress.utils.template(jQuery('#book_progress_create_template').html()),

		events: {
			'change .mbp-progress-today': 'update_progress_today',
			'click .mbp-create-progress-button': 'save_progress',
			'change .mbp-create-progress-date': 'update_progress_date',
			'change .mbp-phase-complete': 'update_phase_progress',
			'click .mbp-phase-complete-button': 'complete_phase',
		},

		initialize: function(options) {
			this.parent = options.parent;
			this.book_phases = new mybookprogress.BookPhasesView({model: this.model});
			this.set_subview('.mbp-book-phases-container .mbp-book-phases', this.book_phases);
			this.listenTo(this.book_phases.collection, 'select', this.change_current_phase);
			this.listenTo(this.model, 'change:phases_progress', this.determine_current_phase);
		},

		render: function() {
			View.prototype.render.call(this);
			utils.datepicker(this.$('.mbp-create-progress-date'));
			this.determine_current_phase();
			this.setup_complete_button();
			return this;
		},

		setup_complete_button: function() {
			var message = mybookprogress_i18n.complete_this_phase;
			var options = {
				position: {
					my: "center bottom-15",
					at: "center top",
					collision: "none",
					using: function(position, feedback) {
						jQuery(this).css(position);
						jQuery("<div>").addClass("arrow center bottom").appendTo(this);
					}
				}
			};
			mybookprogress.utils.tooltip(this.$('.mbp-phase-complete-button'), message, options);
		},

		complete_phase: function() {
			if(this.$('.mbp-phase-complete-button').hasClass('mbp-disabled')) { return; }

			var complete_confirm = jQuery(mybookprogress.utils.template(jQuery('#book_progress_phase_complete_confirm_modal_template').html())({phase_name: this.current_phase.name}));
			complete_confirm.on('click', '.mbp-cancel-complete', function() { tb_remove(); });
			complete_confirm.on('click', '.mbp-confirm-complete', _.bind(function() {
				tb_remove();
				this.$('.mbp-create-progress-button').addClass('mbp-disabled');
				this.$('.mbp-phase-complete-button').addClass('mbp-disabled');
				this.model.update_phase_status(this.current_phase.id, 'complete');
				if(this.model.is_complete()) {
					this.display_fireworks('You finished your book!');
				} else {
					this.display_fireworks('You finished the '+this.current_phase.name+' phase!');
				}
				this.model.save(null, {success: _.bind(function () {
					this.$('.mbp-create-progress-button').removeClass('mbp-disabled');
					this.$('.mbp-phase-complete-button').removeClass('mbp-disabled');
					this.determine_current_phase();
				}, this)});
			}, this));
			utils.modal(complete_confirm);
		},

		determine_current_phase: function() {
			this.current_phase = null;
			var last_progress_entry = this.model.get_progress().at(0);
			if(last_progress_entry) {
				this.current_phase = this.model.get_phase(last_progress_entry.get('phase_id'));
				if(this.current_phase) {
					var phase_status = this.model.get_phase_status(this.current_phase.id);
					if(phase_status == 'complete') { this.current_phase = null; }
				}
			}
			if(!this.current_phase) {
				this.current_phase = this.model.get_next_phase();
			}
			if(!this.current_phase) { return this.$el.hide(); }
			this.current_phase_status = this.model.get_phase_status(this.current_phase.id);
			this.current_phase_progress = this.model.get_phase_progress(this.current_phase.id);

			this.make_new_entry();
		},

		make_new_entry: function() {
			if(this.parent.stats_tab) {
				this.parent.stats_tab.show_phase_stats(this.current_phase['id']);
			}

			var target = this.current_phase.progress_target;
			var progress = this.current_phase_progress;

			if(this.progress_entry) { this.stopListening(this.progress_entry); }
			this.progress_entry = new mybookprogress.ProgressEntry({
				timestamp: utils.unix_timestamp_now(),
				book_id: this.model.id,
				phase_id: this.current_phase.id,
				progress_type: this.current_phase.progress_type,
				progress: progress,
				target: target,
			});
			this.listenTo(this.progress_entry, 'change:progress', this.update_phase_progress);
			this.listenTo(this.progress_entry, 'change:progress', this.validate_progress);

			this.progress_editor = new mybookprogress.BookProgressEditor({model: this.progress_entry});
			this.set_subview('.mbp-progress-editor-container', this.progress_editor);

			this.$('.mbp-progress-today').prop('checked', true);
			this.update_progress_today();

			this.$('.mbp-phase-complete').prop('checked', true);
			this.$('.mbp-reduce-progress').prop('checked', false);
			this.update_phase_progress();

			this.render_current_phase_indicator();
		},

		save_progress: function() {
			if(this.$('.mbp-create-progress-button').hasClass('mbp-disabled')) { return; }
			if(!this.validate_progress()) { return; }
			this.$('.mbp-create-progress-button').addClass('mbp-disabled');
			this.$('.mbp-phase-complete-button').addClass('mbp-disabled');
			this.$('.mbp-create-progress-button').mbp_loading();

			var status = this.progress_entry.get('phase_complete') == true ? 'complete' : 'working';

			if(status !== this.current_phase_status) {
				this.model.update_phase_status(this.current_phase.id, status);
			}

			var notes = '';
			var book_title = this.model.get_title();
			var phase = this.model.get_phase(this.progress_entry.get('phase_id'));

			if(status == 'complete') {
				if(this.model.is_complete()) {
					notes = 'I just finished '+phase['name']+'! Woohoo!';
					this.display_fireworks('You finished your book!', 'large');
				} else {
					var next_phase = this.model.get_next_phase();
					notes = 'I just completed the '+phase['name']+' phase of '+book_title+'! Now onto '+next_phase['name']+'!';
					this.display_fireworks('You finished the '+this.current_phase.name+' phase!', 'large');
				}
			} else {
				var phase_progress = utils.progress_format(this.progress_entry.get('progress')*100);
				notes = 'I just made progress on '+book_title+'! So far I\'m '+phase_progress+'% complete on the '+phase['name']+' phase.';
				if(phase['deadline']) {	notes += ' '+utils.human_time_diff(utils.unix_timestamp_now()-((new Date()).getTimezoneOffset()*60), phase['deadline'])+' remain until the deadline.'; }

				var progress_diff = this.progress_entry.get('progress')*this.progress_entry.get('target') - this.current_phase_progress*this.current_phase.progress_target;
				var progress_type = mybookprogress.progress_types[this.progress_entry.get('progress_type')];
				var message = '';
				if(progress_type && progress_diff > 0) {
					var progress_diff_format = utils.progress_format(progress_diff);
					var progress_units = progress_diff_format == '1' ? utils.uncapitalize(progress_type.unit) : utils.uncapitalize(progress_type.units);
					message = 'You are '+progress_diff_format+' '+progress_units+' closer to your goal!';
				}
				this.display_fireworks(message);

				if(this.progress_entry.get('target') != this.current_phase.progress_target) {
					this.current_phase.progress_target = this.progress_entry.get('target');
					this.model.update_phase(this.current_phase);
				}
			}

			this.progress_entry.set('notes', notes);

			var finish = _.bind(function () {
				this.progress_entry.save(null, {success: _.bind(function() {
					this.progress_entry.mbp_is_new = true;
					this.model.get_progress().add(this.progress_entry);
					this.$('.mbp-create-progress-button').removeClass('mbp-disabled');
					this.$('.mbp-phase-complete-button').removeClass('mbp-disabled');
					this.$('.mbp-create-progress-button').mbp_loading('destroy');
					this.determine_current_phase();
				}, this)});
			}, this);

			if(this.model.is_dirty) { this.model.save(null, {success: finish}); } else { finish(); }
		},

		change_current_phase: function(view) {
			var new_phase_status = this.model.get_phase_status(view.model.id);
			if(new_phase_status !== 'complete') {
				this.current_phase = this.model.get_phase(view.model.id);
				this.current_phase_status = new_phase_status;
				this.current_phase_progress = this.model.get_phase_progress(view.model.id);
				this.make_new_entry();
			}
		},

		validate_progress: function() {
			var progress_equal = utils.number_format(this.current_phase_progress, 7) === utils.number_format(this.progress_entry.get('progress'), 7);
			var target_equal = utils.number_format(this.current_phase.progress_target, 7) === utils.number_format(this.progress_entry.get('target'), 7);

			if(this.progress_entry.get('progress') < 1 && target_equal && progress_equal) {
				this.$('.mbp-create-progress-errors').text('Please input your new progress value');
				this.$('.mbp-create-progress-errors').show();
				return false;
			}

			if(target_equal && this.current_phase_progress > this.progress_entry.get('progress') && !this.$('.mbp-reduce-progress').is(':checked')) {
				this.$('.mbp-create-progress-errors').hide();
				return false;
			}

			this.$('.mbp-create-progress-errors').hide();
			return true;
		},

		render_current_phase_indicator: function() {
			var current_phase_view = null;
			for(var i = this.book_phases.item_views.length - 1; i >= 0; i--) {
				if(this.book_phases.item_views[i].model.id == this.current_phase.id) { current_phase_view = this.book_phases.item_views[i]; break; }
			}
			if(current_phase_view) {
				setTimeout(_.bind(function() {
					this.$('.mbp-book-phase-indicator').show().css({
						top: this.$('.mbp-create-progress-section').offset().top-this.$el.offset().top,
						left: Math.max(window.innerWidth <= 1500 ? 100 : 120, current_phase_view.$el.offset().left-this.$el.offset().left+current_phase_view.$el.outerWidth()*0.5),
					});
				}, this), 0);
			}
		},

		update_progress_today: function() {
			if(this.$('.mbp-progress-today').is(':checked')) {
				this.progress_entry.set('timestamp', utils.unix_timestamp_now());
				this.$('.mbp-create-progress-date').removeClass('error');
				this.$('.mbp-create-progress-date-error').empty();
				this.$('.mbp-create-progress-date-container').hide(300);
			} else {
				this.$('.mbp-create-progress-date').val(utils.format_date(this.progress_entry.get('timestamp')));
				this.$('.mbp-create-progress-date-container').show(300);
			}
		},

		update_phase_progress: function() {
			if(this.progress_entry.get('progress') == 1) {
				if(!this.$('.mbp-phase-complete-container').is(':visible')) { this.$('.mbp-phase-complete-container').show(300); }
				if(this.$('.mbp-phase-complete').is(':checked')) {
					this.$('.mbp-phase-complete-container').removeClass('mbp-not-complete');
					this.progress_entry.set('phase_complete', true);
				} else {
					this.$('.mbp-phase-complete-container').addClass('mbp-not-complete');
					this.progress_entry.set('phase_complete', false);
				}
			} else {
				if(this.$('.mbp-phase-complete-container').is(':visible')) { this.$('.mbp-phase-complete-container').hide(300); }
				this.$('.mbp-phase-complete-container').removeClass('mbp-not-complete');
				this.progress_entry.set('phase_complete', false);
			}

			var target_equal = utils.number_format(this.current_phase.progress_target, 7) === utils.number_format(this.progress_entry.get('target'), 7);
			if(target_equal && this.current_phase_progress > this.progress_entry.get('progress')) {
				if(!this.$('.mbp-reduce-progress-container').is(':visible')) { this.$('.mbp-reduce-progress-container').show(300); }
			} else {
				if(this.$('.mbp-reduce-progress-container').is(':visible')) { this.$('.mbp-reduce-progress-container').hide(300); }
			}
		},

		update_progress_date: function() {
			var error = '';
			var date = utils.parse_date(this.$('.mbp-create-progress-date').val());
			if(typeof date === 'number') {
				if(date > utils.unix_timestamp_now()) {
					error = 'Don\'t get ahead of yourself!<br>Choose a date in the past.';
				} else {
					this.progress_entry.set('timestamp', date);
					this.$('.mbp-create-progress-date').removeClass('mbp-error');
					this.$('.mbp-create-progress-date-error').empty();
				}
			} else {
				error = date;
			}

			if(error) {
				this.$('.mbp-create-progress-date').addClass('mbp-error');
				this.$('.mbp-create-progress-date-error').html(error);
			}
		},

		display_fireworks: function(message, size) {
			var launch_rate = {min: 5, max: 60};
			var time = 3000;
			if(size === 'large') {
				launch_rate = 60;
				time = 6000;
			}

			var el = jQuery('<canvas class="mbp-admin-page-fireworks"></canvas>');
			jQuery('body').append(el);
			el.fireworks({launch_rate: launch_rate});
			setTimeout(function() {
				el.fireworks('stop');
			}, time);
			setTimeout(function() {
				el.fireworks('destroy');
				el.remove();
			}, time + 3000);

			if(message) {
				var message_el = jQuery('<div class="mbp-admin-page-fireworks-message"><div class="mbp-admin-page-fireworks-message-congrats">Congratulations!</div><div class="mbp-admin-page-fireworks-message-text"></div></div>');
				message_el.find('.mbp-admin-page-fireworks-message-text').text(message);
				jQuery('body').append(message_el);
				message_el.fadeIn(500);
				setTimeout(function() {
					message_el.fadeOut(500);
				}, time + 1000);
				setTimeout(function() {
					message_el.remove();
				}, time + 1500);
			}
		},
	});

	/*---------------------------------------------------------*/
	/* Progress Timeline                                       */
	/*---------------------------------------------------------*/

	mybookprogress.BookProgressTimelineEntry = View.extend({
		className: 'mbp-progress mbp-progress-entry',
		template: mybookprogress.utils.template(jQuery('#book_progress_entry_template').html()),

		events: {
			'click .mbp-progress-inner': 'select',
			'input .mbp-progress-notes': 'notes_updated',
			'click .mbp-edit-progress-button': 'start_editing',
			'click .mbp-delete-progress-button': 'delete',
			'click .mbp-save-progress-button': 'stop_editing',
			'click .mbp-share-progress-more-button': 'toggle_share_more',
			'click .mbp-share-progress-button:not(.mbp-share-progress-more-button)': 'share_progress',
			'change .mbp-progress-date-editor': 'date_updated',
			'input .mbp-progress-date-editor': 'date_updated',
		},

		initialize: function() {
			this.set_subview('.mbp-progress-display-container', new mybookprogress.BookProgressDisplay({model: this.model}));

			this.selected = false;
			if(this.model.mbp_is_new) { this.selected = true; }

			this.notes_timer = null;
			this.editing = false;
		},

		render: function() {
			this.$el.html(this.template());
			this.render_subviews();
			this.render_extra();
			this.render_selected();
			this.render_progress_sharing_buttons();
			return this;
		},

		render_extra: function() {
			if(this.model.get('phase_complete')) {
				this.$el.addClass('mbp-finished-phase');
				this.$('.mbp-progress-extra').html('Finished<br>'+this.model.get('phase_name')+'!');
			} else {
				this.$el.removeClass('mbp-finished-phase');
			}
		},

		render_selected: function() {
			if(this.selected) {
				this.$el.addClass('mbp-progress-selected');
				if(this.model.mbp_is_new) { this.$('.mbp-progress-sharing-message').show(); }
				this.$('.mbp-edit-progress-button').show();
				this.$('.mbp-progress-sharing').show();
			} else {
				this.$el.removeClass('mbp-progress-selected');
				this.$('.mbp-progress-sharing-message').hide();
				this.$('.mbp-edit-progress-button').hide();
				this.$('.mbp-progress-sharing').hide();
				this.$('.mbp-share-progress-more-menu').hide();
			}
		},

		render_progress_sharing_buttons: function() {
			this.$('.mbp-share-progress-buttons').empty();
			var normals = _.clone(mybookprogress.progress_sharing_types);

			var more = jQuery('<div class="mbp-share-progress-more"><div class="mbp-share-progress-button mbp-share-progress-more-button">'+mybookprogress_i18n.more+'</div><div class="mbp-share-progress-more-menu"></div></div>');
			var hasmore = false;

			for(type_name in mybookprogress.progress_sharing_types) {
				var type = mybookprogress.progress_sharing_types[type_name];
				var parent = type.default ? this.$('.mbp-share-progress-buttons') : more.find('.mbp-share-progress-more-menu');
				if(!type.default) { hasmore = true; }
				var text = typeof type.button === 'string' ? type.button : type.button(this.model, type);
				parent.append('<div class="mbp-share-progress-button" data-mbp-progress-sharing-type="'+type_name+'">'+text+'</div>');
			}

			if(hasmore) {
				this.$('.mbp-share-progress-buttons').append(more);
			}
		},

		share_progress: function(e) {
			this.stop_editing();
			var button = jQuery(e.target);
			var type_name = button.attr('data-mbp-progress-sharing-type');
			var type = mybookprogress.progress_sharing_types[type_name];
			if(type) { type.share(this.model, button, type); }
		},

		select: function() {
			if(this.selected) { return; }
			this.selected = true;
			this.render_selected();
		},

		deselect: function(e) {
			if(!this.selected) { return; }
			if(this.$('.mbp-progress-inner').is(':hover')) { return; }
			this.selected = false;
			if(this.model.mbp_is_new) { delete this.model.mbp_is_new; }
			this.stop_editing();
			this.render_selected();
		},

		delete: function() {
			if(this.$('.mbp-delete-progress-button').hasClass('mbp-disabled')) { return; }
			var delete_confirm = jQuery(mybookprogress.utils.template(jQuery('#book_progress_delete_confirm_modal_template').html())());
			delete_confirm.on('click', '.mbp-cancel-delete', function() { tb_remove(); });
			delete_confirm.on('click', '.mbp-confirm-delete', _.bind(function() {
				tb_remove();
				this.$('.mbp-delete-progress-button').addClass('mbp-disabled');
				this.$('.mbp-delete-progress-button').mbp_loading();
				this.model.destroy();
			}, this));
			utils.modal(delete_confirm);
		},

		start_editing: function() {
			if(!this.selected || this.editing) { return; }
			this.editing = true;
			this.$('.mbp-progress-edit').html('<div class="mbp-save-progress-button">'+mybookprogress_i18n.save+'</div><div class="mbp-delete-progress-button">&nbsp;</div>');
			this.set_subview('.mbp-progress-display-container', new mybookprogress.BookProgressEditor({model: this.model}));

			var width = this.$('.mbp-progress-date').width();
			this.$('.mbp-progress-date').html('<input type="text" class="mbp-progress-date-editor">');
			var dateeditor = this.$('.mbp-progress-date-editor');
			dateeditor.val(utils.format_date(this.model.get('timestamp'))).width(width);
			utils.datepicker(dateeditor);
		},

		stop_editing: function() {
			if(!this.editing) { return; }

			this.editing = false;
			this.save_model();

			this.$('.mbp-progress-date').text(this.format_date());
			this.$('.mbp-progress-edit').html('<div class="mbp-edit-progress-button">'+mybookprogress_i18n.edit+'</div>');
			this.set_subview('.mbp-progress-display-container', new mybookprogress.BookProgressDisplay({model: this.model}));
		},

		date_updated: function() {
			var dateeditor = this.$('.mbp-progress-date-editor');
			var date = utils.parse_date(dateeditor.val());
			if(typeof date === 'string') {
				dateeditor.addClass('mbp-error');
			} else {
				this.model.set('timestamp', date);
				dateeditor.removeClass('mbp-error');
			}
		},

		notes_updated: function() {
			this.model.set('notes', this.$('.mbp-progress-notes').val());
			if(this.save_timer) { clearInterval(this.save_timer); }
			this.save_timer = setTimeout(_.bind(this.save_model, this), 1000);
		},

		save_model: function() {
			if(this.save_timer) { clearInterval(this.save_timer); this.save_timer = null; }
			this.model.save();
		},

		format_date: function() {
			return utils.format_date(this.model.get('timestamp'));
		},

		format_phase: function() {
			return this.model.get('phase_name');
		},

		toggle_share_more: function() {
			if(this.$('.mbp-share-progress-more-menu').is(':visible')) {
				this.$('.mbp-share-progress-more-menu').hide(300);
			} else {
				this.$('.mbp-share-progress-more-menu').show(300);
			}
		},
	});

	mybookprogress.BookProgressTimelineMoreButton = View.extend({
		className: 'mbp-progress mbp-progress-more-button',
		template: mybookprogress.utils.template(jQuery('#book_progress_more_template').html()),

		events: {
			'click': 'clicked',
		},

		clicked: function() {
			if(this.loading) { return; }
			this.loading = true;
			this.$el.addClass('mbp-loading');
			this.collection.next_page();
		}
	});

	mybookprogress.BookProgressTimelineHeader = View.extend({
		className: 'mbp-progress mbp-progress-header',
		template: mybookprogress.utils.template(jQuery('#book_progress_header_template').html()),

		initialize: function(options) {
			this.message = options.message;
		}
	});

	mybookprogress.BookProgressTimelineNewBook = View.extend({
		className: 'mbp-progress mbp-progress-new-book',
		template: mybookprogress.utils.template(jQuery('#book_progress_newbook_template').html()),

		format_date: function() {
			return utils.format_date(this.model.get('created'));
		},
	});

	mybookprogress.BookProgressTimelineView = CollectionView.extend({
		item_view: mybookprogress.BookProgressTimelineEntry,

		initialize: function(options) {
			this.collection = this.model.get_progress();
			CollectionView.prototype.initialize.call(this, options);
			jQuery(document).on('click', _.bind(this.deselect, this));
		},

		render: function() {
			this.$el.empty();

			var current_month = new Date(new Date().getFullYear(), new Date().getMonth(), 1);
			var num_since_header = 0;

			_.each(this.item_views, function(view) {
				if(view.model.get('timestamp') < utils.unix_timestamp(current_month)) {
					var year = current_month.getFullYear();
					var month = current_month.getMonth()-1;
					if(month < 0) { year -= 1; month += 12; }
					current_month = new Date(year, month, 1);
					if(num_since_header >= 3) {
						num_since_header = 0;
						var with_year = new Date().getFullYear() !== year;
						this.$el.append(new mybookprogress.BookProgressTimelineHeader({message: utils.month_name(month)+(with_year ? ' '+year : '')}).render().el);
					}
				}
				num_since_header++;
				this.$el.append(view.setElement(view.el).render().el);
			}, this);

			if(this.collection.has_more_pages()) {
				this.$el.append(new mybookprogress.BookProgressTimelineMoreButton({collection: this.collection}).render().el);
			} else if(this.collection.length) {
				this.$el.append(new mybookprogress.BookProgressTimelineNewBook({model: this.model}).render().el);
			}

			return this;
		},

		deselect: function(e) {
			if(jQuery('.mbp-ui-datepicker').length && jQuery('.mbp-ui-datepicker').is(':hover')) { return; }
			_.each(this.item_views, function(view) { view.deselect(e); });
		},
	});

	/*---------------------------------------------------------*/
	/* Overview                                                */
	/*---------------------------------------------------------*/

	mybookprogress.OverviewBookView = View.extend({
		className: 'mbp-book-phases-container',
		template: mybookprogress.utils.template(jQuery('#book_overview_book_template').html()),

		initialize: function() {
			this.set_subview('.mbp-book-phases', new mybookprogress.BookPhasesView({model: this.model}));
		},
	});

	mybookprogress.OverviewView = View.extend({
		template: mybookprogress.utils.template(jQuery('#book_overview_template').html()),

		initialize: function() {
			this.set_subview('.mbp-progress-view-container', new mybookprogress.BookProgressViewView());
			this.set_subview('.mbp-book-tabs', new mybookprogress.BookProgressTabsView({parent: this}));
			this.set_subview('.mbp-all-books-section .mbp-section-content', new CollectionView({collection: books, item_view: mybookprogress.OverviewBookView}));
		},
	});

})();
