(function () {
	var utils = mybookprogress.utils;
	var settings = null;
	var books = null;
	var phase_templates = null;
	var progress_tab = null;
	var mbt_books = null;

	mybookprogress.on('models_loaded', function() {
		settings = mybookprogress.settings;
		books = mybookprogress.books;
		phase_templates = mybookprogress.phase_templates;
		mbt_books = mybookprogress.mbt_books;
	});
	mybookprogress.on('loaded', function() {
		progress_tab = mybookprogress.progress_tab;
	});

	var View = mybookprogress.View;
	var CollectionView = mybookprogress.CollectionView;
	var ModalView = mybookprogress.ModalView;
	var VirtualModel = mybookprogress.VirtualModel;
	var VirtualCollection = mybookprogress.VirtualCollection;

	/*---------------------------------------------------------*/
	/* Book Setup View                                         */
	/*---------------------------------------------------------*/

	mybookprogress.BookSetupView = View.extend({
		template: mybookprogress.utils.template(jQuery('#book_setup_template').html()),

		events: {
			'click .mbp-save-book-button': 'save_book',
			'click .mbp-back-button': 'discard_book',
			'click .mbp-delete-book-button': 'delete_book',
		},

		initialize: function() {
			this.editing_model = new mybookprogress.Book(jQuery.extend(true, {}, this.model.attributes));
			this.set_subview('.mbp-mybooktable-step', new mybookprogress.BookSetupMyBookTableView({model: this.editing_model}));
			this.set_subview('.mbp-book-title-step', new mybookprogress.BookSetupTitleView({model: this.editing_model}));
			this.set_subview('.mbp-book-phases-step', new mybookprogress.BookSetupPhasesView({model: this.editing_model}));
			this.set_subview('.mbp-book-display-step', new mybookprogress.BookSetupDisplayView({model: this.editing_model}));
		},

		render: function() {
			this.$el.html(this.template());
			this.render_subviews();
			this.render_buttons();
			return this;
		},

		render_buttons: function() {
			if(books.length === 0) {
				this.$('.mbp-back-button-container').hide();
			}
			if(this.model.isNew()) {
				this.$('.mbp-delete-book-button-container').hide();
			} else {
				this.$('.mbp-delete-book-button-container').show();
			}
		},

		delete_book: function() {
			if(this.$('.mbp-delete-book-button').hasClass('mbp-disabled')) { return; }
			var delete_confirm = jQuery(mybookprogress.utils.template(jQuery('#book_setup_delete_confirm_modal_template').html())());
			delete_confirm.on('click', '.mbp-cancel-delete', function() { tb_remove(); });
			delete_confirm.on('click', '.mbp-confirm-delete', _.bind(function() {
				tb_remove();
				this.$('.mbp-back-button').addClass('mbp-disabled');
				this.$('.mbp-save-book-button').addClass('mbp-disabled');
				this.$('.mbp-delete-book-button').addClass('mbp-disabled');
				this.$('.mbp-delete-book-button').mbp_loading();
				this.model.destroy({success: function() {
					progress_tab.show_current_book();
					window.scrollTo(0, 0);
				}});
			}, this));
			utils.modal(delete_confirm);
		},

		discard_book: function() {
			if(this.$('.mbp-back-button').hasClass('mbp-disabled')) { return; }
			if(this.editing_model.is_dirty) {
				var discard_confirm = jQuery(mybookprogress.utils.template(jQuery('#book_setup_discard_confirm_modal_template').html())());
				discard_confirm.on('click', '.mbp-save-changes', _.bind(function() {
					tb_remove();
					this.save_book();
				}, this));
				discard_confirm.on('click', '.mbp-discard-changes', _.bind(function() {
					tb_remove();
					this.book_discarded();
				}, this));
				utils.modal(discard_confirm);
			} else {
				this.book_discarded();
			}
		},

		book_discarded: function() {
			if(this.model.isNew()) {
				progress_tab.show_current_book();
			} else {
				progress_tab.toggle_setup();
			}
		},

		save_book: function() {
			if(this.$('.mbp-save-book-button').hasClass('mbp-disabled')) { return; }
			this.$('.mbp-back-button').addClass('mbp-disabled');
			this.$('.mbp-save-book-button').addClass('mbp-disabled');
			this.$('.mbp-delete-book-button').addClass('mbp-disabled');
			this.$('.mbp-save-book-button').mbp_loading();
			var callback = this.model.isNew() ? this.book_created : this.book_saved;
			this.model.save(this.editing_model.attributes, {success: _.bind(callback, this)});
			mbt_books.update_book(this.model);
		},

		book_created: function(book) {
			books.add(book);
			progress_tab.show_book(book.id);
		},

		book_saved: function() {
			progress_tab.toggle_setup();
			window.scrollTo(0, 0);
		},
	});

	/*---------------------------------------------------------*/
	/* MyBookTable Step                                        */
	/*---------------------------------------------------------*/

	mybookprogress.BookSetupMyBookTableView = View.extend({
		template: mybookprogress.utils.template(jQuery('#book_setup_mybooktable_step_template').html()),

		events: {
			'change .mbp-mybooktable-books': 'update_book',
		},

		render: function() {
			this.$el.html(this.template());
			this.render_books();
			return this;
		},

		render_books: function() {
			var books = this.$('.mbp-mybooktable-books');
			books.append(jQuery('<option value="0">-- '+mybookprogress_i18n.choose_one+' --</option>'));
			var current_book = this.model.get('mbt_book');
			var has_books = false;
			mbt_books.each(_.bind(function(book) {
				if(book.get('mbp_book') && book.get('mbp_book') !== this.model.id) { return; }
				has_books = true;
				books.append(jQuery('<option value="'+book.id+'"'+(current_book === book.id ? ' selected="selected"' : '')+'>'+book.get_title()+'</option>'));
			}, this));
			if(!has_books) {
				this.$el.hide();
			} else {
				this.$el.show();
			}
		},

		update_book: function() {
			var value = parseInt(this.$('.mbp-mybooktable-books').val());
			this.model.set('mbt_book', value > 0 ? value : null);
		},
	});

	/*---------------------------------------------------------*/
	/* Title Step                                              */
	/*---------------------------------------------------------*/

	mybookprogress.BookSetupTitleView = View.extend({
		template: mybookprogress.utils.template(jQuery('#book_setup_title_step_template').html()),

		events: {
			'input .mbp-book-title': 'update_title',
		},

		initialize: function() {
			this.listenTo(this.model, 'change:mbt_book', this.update_mbt_book);
		},

		render: function() {
			this.$el.html(this.template());
			this.render_title();
			return this;
		},

		render_title: function() {
			this.$('.mbp-book-title').val(this.model.get('title'));
		},

		update_mbt_book: function() {
			var title = this.model.get('title');

			var old_mbt_book = this.model.previous('mbt_book') ? mbt_books.get(this.model.previous('mbt_book')) : null;
			var old_mbt_book_title = old_mbt_book ? old_mbt_book.get('title') : null;

			var mbt_book = this.model.get('mbt_book') ? mbt_books.get(this.model.get('mbt_book')) : null;
			var mbt_book_title = mbt_book ? mbt_book.get('title') : null;

			if(!title || title == old_mbt_book_title) {
				title = mbt_book_title;
				this.model.set('title', title);
				this.render_title();
			}
		},

		update_title: function() {
			this.model.set('title', this.$('.mbp-book-title').val());
		},
	});

	/*---------------------------------------------------------*/
	/* Book Phases Step                                        */
	/*---------------------------------------------------------*/

	mybookprogress.BookSetupPhasesView = View.extend({
		template: mybookprogress.utils.template(jQuery('#book_setup_phase_step_template').html()),

		initialize: function() {
			this.set_subview('.mbp-book-phase-template-container', new mybookprogress.BookSetupPhaseTemplatesView({model: this.model}));
			this.phase_editor = new mybookprogress.BookSetupPhaseEditorView({model: this.model});
			this.listenTo(this.phase_editor, 'change_current_phase', this.change_current_phase);
			this.listenTo(this.phase_editor, 'change_phases_length', this.render_current_phase_indicator);
			this.set_subview('.mbp-book-phases-editor', this.phase_editor);
			this.change_current_phase();
		},

		change_current_phase: function() {
			this.set_subview('.mbp-book-phase-details-section', new mybookprogress.BookSetupPhaseDetailView({model: this.phase_editor.current_phase, book: this.model}));
			this.render_current_phase_indicator();
		},

		render_current_phase_indicator: function() {
			var current_phase_view = this.phase_editor.book_phases_view.model_view(this.phase_editor.current_phase);
			if(current_phase_view) {
				setTimeout(_.bind(function() {
					this.$('.mbp-book-phase-indicator').show().css({
						top: this.$('.mbp-book-phase-details-section').offset().top-this.$el.offset().top,
						left: Math.max(window.innerWidth <= 1500 ? 80 : 100, current_phase_view.$el.offset().left-this.$el.offset().left+current_phase_view.$el.outerWidth()*0.5),
					});
				}, this), 0);
			}
		},

		render: function() {
			View.prototype.render.call(this);
			this.render_current_phase_indicator();
			return this;
		},
	});

	/*---------------------------------------------------------*/
	/* Phase Template Selector                                 */
	/*---------------------------------------------------------*/

	mybookprogress.BookSetupPhaseTemplatesView = View.extend({
		template: mybookprogress.utils.template(jQuery('#book_setup_phase_template_template').html()),

		events: {
			'change .mbp-book-phase-template': 'phase_template_change',
			'click .mbp-template-manager-button': 'manage_templates',
		},

		initialize: function() {
			this.listenTo(this.model, 'change:phases', this.render_options);
			this.listenTo(phase_templates, 'change add remove reset sort', this.render_options);
		},

		render: function() {
			this.$el.html(this.template());
			this.render_options();
			return this;
		},

		render_options: function() {
			var template_id = this.model.get('phases');
			if(typeof template_id !== 'string') {
				template_id = null;
			} else if(!phase_templates.get(template_id)) {
				template_id = phase_templates.at(0).id;
				this.model.set({phases: phase_templates.at(0).id, phases_status: {}});
			}

			var select = this.$('.mbp-book-phase-template');
			select.empty();
			select.append(jQuery('<option value="custom" class="mbp-book-phase-template-custom" '+(template_id ? '' : ' selected="selected"')+'>'+mybookprogress_i18n.custom+'</option>'));
			phase_templates.each(function(template) {
				select.append(jQuery('<option value="'+template.id+'"'+(template_id === template.id ? ' selected="selected"' : '')+'>'+template.get('name')+'</option>'));
			}, this);
		},

		phase_template_change: function() {
			var template = this.$('.mbp-book-phase-template').val();
			if(template === 'custom') {
				if(typeof this.model.get('phases') === 'string') {
					this.model.set({phases: phase_templates.get(this.model.get('phases')).get('phases')});
				}
			} else {
				this.model.set({phases: template, phases_status: {}});
			}
		},

		manage_templates: function() {
			var modal = new mybookprogress.PhaseTemplateManagerView({model: this.model});
		},
	});

	/*---------------------------------------------------------*/
	/* Phase Template Manager                                  */
	/*---------------------------------------------------------*/

	mybookprogress.PhaseTemplateManagerView = ModalView.extend({
		template: mybookprogress.utils.template(jQuery('#book_setup_manage_templates_modal_template').html()),

		events: {
			'click .mbp-save-current': 'save_current',
			'click .mbp-close': 'close',
		},

		initialize: function(options) {
			ModalView.prototype.initialize.call(this, options);

			this.set_subview('.mbp-phase-templates', new CollectionView({collection: phase_templates, item_view: mybookprogress.PhaseTemplate}));
		},

		save_current: function() {
			var new_name = mybookprogress_i18n.new_template;
			var i = 2;
			while(phase_templates.where({name: new_name}).length) {
				new_name = mybookprogress_i18n.new_template+' '+i++;
			}

			var new_id = new Date().getTime();
			while(phase_templates.get(new_id.toString())) { new_id++; }
			new_id = new_id.toString();

			phase_templates.create({id: new_id, name: new_name, phases: this.model.get_phases()});
			this.model.set('phases', new_id);
		},
	});

	mybookprogress.PhaseTemplate = View.extend({
		className: 'mbp-phase-template',
		template: mybookprogress.utils.template(jQuery('#book_setup_phase_template_item_template').html()),

		events: {
			'click .mbp-template-delete': 'destroy',
			'input .mbp-template-name': 'updated',
			'blur .mbp-template-name': 'save',
		},

		bindings: {
			'name .mbp-template-name' : '',
		},

		updated: function() {
			if(this.update_timeout) { clearTimeout(this.update_timeout); }
			this.update_timeout = setTimeout(_.bind(this.save, this), 1000);
		},

		save: function() {
			if(this.update_timeout) {
				clearTimeout(this.update_timeout);
				this.update_timeout = null;
				this.model.save();
			}
		},

		destroy: function() {
			this.model.destroy();
		},
	});

	/*---------------------------------------------------------*/
	/* Phase Editor                                            */
	/*---------------------------------------------------------*/

	mybookprogress.BookSetupPhaseEditorView = View.extend({
		template: mybookprogress.utils.template(jQuery('#book_setup_phase_editor_template').html()),

		events: {
			'click .mbp-book-phase-adder': 'add_phase',
		},

		initialize: function() {
			this.max_phases = 7;
			if(!this.model.get_phases().length) { this.model.set({phases: phase_templates.at(0).id, phases_status: {}}); }
			this.load_phases();
			this.listenTo(this.model, 'change:phases', this.updated_phases);
		},

		render: function() {
			View.prototype.render.call(this);
			this.render_phases_length();
		},

		render_phases_length: function() {
			if(this.phase_collection.length > 4) {
				this.$('.mbp-book-phases').addClass('mbp-small-phases');
				this.$('.mbp-book-phase-adder').addClass('mbp-small-phases');
			 } else {
			 	this.$('.mbp-book-phases').removeClass('mbp-small-phases');
				this.$('.mbp-book-phase-adder').removeClass('mbp-small-phases');
			 }
			if(this.phase_collection.length >= this.max_phases) {
				this.$('.mbp-book-phase-adder').addClass('mbp-disabled');
			} else {
				this.$('.mbp-book-phase-adder').removeClass('mbp-disabled');
			}
		},

		load_phases: function() {
			var is_new = !Boolean(this.phase_collection);
			if(!is_new) {
				this.stopListening(this.phase_collection);
				this.stopListening(this.current_phase);
			}

			this.phase_collection = new VirtualCollection(this.model.get_phases(), {model: mybookprogress.BookSetupPhaseItemModel, comparator: 'index'});
			if(!this.phase_collection.length) { this.add_phase(); this.update_phases(); }

			this.book_phases_view = new CollectionView({
				collection: this.phase_collection,
				item_view: mybookprogress.BookSetupPhaseItemView,
				sortable: {
					sorting_attr: 'index',
					handle: '.mbp-book-phase-mover',
					placeholder: 'mbp-book-phase-placeholder'
				},
			});
			this.set_subview('.mbp-book-phases', this.book_phases_view);

			this.listenTo(this.phase_collection, 'change add remove sort', this.update_phases);
			this.listenTo(this.phase_collection, 'select', this.phase_selected);
			this.listenTo(this.phase_collection, 'change add remove', this.change_phases_length);
			this.change_phases_length();

			this.current_phase = null;
			for(var i = 0; i < this.phase_collection.models.length; i++) {
				var status = this.model.get_phase_status(this.phase_collection.models[i].id);
				if(status !== 'complete') {
					this.current_phase = this.phase_collection.models[i];
					break;
				}
			}
			if(!this.current_phase) { this.current_phase = this.phase_collection.at(0); }
			this.listenTo(this.current_phase, 'destroy', this.current_phase_destroyed);
			this.trigger('change_current_phase');
		},

		phase_selected: function(view) {
			this.update_current_phase(view.model.get('index'));
		},

		change_phases_length: function() {
			this.render_phases_length();
			this.trigger('change_phases_length');
		},

		update_current_phase: function(new_phase_index) {
			this.stopListening(this.current_phase);
			this.current_phase = this.phase_collection.at(new_phase_index);
			this.listenTo(this.current_phase, 'destroy', this.current_phase_destroyed);
			this.trigger('change_current_phase');
		},

		current_phase_destroyed: function() {
			this.update_current_phase(this.current_phase.get('index') == 0 ? this.current_phase.get('index')+1 : this.current_phase.get('index')-1);
		},

		add_phase: function() {
			if(this.phase_collection.length < this.max_phases) {
				this.phase_collection.create({index: this.phase_collection.length});
			}
		},

		update_phases: function() {
			var phases = [];
			var phase_ids = [];
			this.phase_collection.each(function(phase) {
				var phase = _.clone(phase.attributes);
				delete phase.index;
				phases.push(phase);
				phase_ids.push(phase.id);
			});

			var phases_status = jQuery.extend(true, {}, this.model.get('phases_status'));
			for(phase_id in phases_status) {
				if(!_.indexOf(phase_ids, phase_id)) { delete phases_status[phase_id]; }
			}
			this.model.set('phases_status', phases_status);

			this.model.set({phases: phases}, {from_phase_editor: true});
		},

		updated_phases: function(model, value, options) {
			if(options.from_phase_editor) { return; }
			this.load_phases();
		},
	});

	mybookprogress.BookSetupPhaseItemView = View.extend({
		className: 'mbp-book-phase',
		template: mybookprogress.utils.template(jQuery('#book_setup_phase_item_template').html()),
		events: {
			'click': 'select',
			'click .mbp-book-phase-remover': 'remover',
			'input .mbp-book-phase-name': 'update_name',
		},

		remover: function() {
			if(this.model.collection.length > 1) {
				this.model.destroy();
			}
		},

		update_name: function() {
			this.model.set('name', this.$('.mbp-book-phase-name').val());
		},

		select: function() {
			this.model.trigger('select', this);
		},
	});

	mybookprogress.BookSetupPhaseItemModel = VirtualModel.extend({
		defaults: {
			index: 0,
			name: '',
			deadline: null,
			progress_type: null,
			progress_target: 1,
		},

		initialize: function() {
			if(!mybookprogress.progress_types[this.get('progress_type')]) {
				this.change_progress_type(mybookprogress.default_progress_type);
			}
			if(!this.get('id')) { this.set('id', this.gen_id()); }
		},

		gen_id: function() {
			var id = new Date().getTime();
			while(this.collection.get(id)) { id++; }
			return id;
		},

		change_progress_type: function(progress_type) {
			var new_progress_type = mybookprogress.progress_types[progress_type];
			if(new_progress_type.default_target) {
				var old_progress_type = mybookprogress.progress_types[this.get('progress_type')];
				if(!old_progress_type || (old_progress_type.default_target && this.get('progress_target') === old_progress_type.default_target)) {
					this.set('progress_target', new_progress_type.default_target);
				}
			}

			this.set('progress_type', progress_type);
		},
	});

	/*---------------------------------------------------------*/
	/* Phase Details                                           */
	/*---------------------------------------------------------*/

	mybookprogress.BookSetupPhaseDetailView = View.extend({
		template: mybookprogress.utils.template(jQuery('#book_setup_phase_details_template').html()),
		events: {
			'click .mbp-phase-progress-type-button': 'update_progress_type',
			'click .mbp-phase-complete': 'update_phase_complete',
		},

		bindings: {
			'deadline .mbp-phase-deadline': 'unformat_deadline format_deadline',
			'progress_target .mbp-phase-progress-target': 'unformat_progress_target',
		},

		initialize: function(options) {
			this.book = options.book;
			this.listenTo(this.model, 'change:name', this.render);
			this.listenTo(this.model, 'change:progress_type', this.render_progress_types);
			this.listenTo(this.model, 'change:progress_type', this.render_progress_target);
		},

		render: function() {
			this.$el.html(this.template());
			this.render_bindings();

			this.render_phase_complete();
			this.render_controls();
			this.render_progress_types();
			this.render_progress_target();
			return this;
		},

		update_phase_complete: function() {
			status = this.$('.mbp-phase-complete').is(':checked') ? 'complete' : '';
			this.book.update_phase_status(this.model.id, status);
		},

		render_phase_complete: function() {
			var status = this.book.get_phase_status(this.model.id);
			this.$('.mbp-phase-complete').prop('checked', status == 'complete');
		},

		render_controls: function() {
			utils.datepicker(this.$('.mbp-phase-deadline'));
		},

		render_progress_types: function() {
			this.$('.mbp-phase-progress-types').empty();
			for(type in mybookprogress.progress_types) {
				var new_button = jQuery('<div class="mbp-setting-button mbp-phase-progress-type-button"></div>');
				new_button.attr('data-mbp-progress-type', type);
				new_button.text(mybookprogress.progress_types[type].name);
				if(type === this.model.get('progress_type')) { new_button.addClass('mbp-selected'); }
				this.$('.mbp-phase-progress-types').append(new_button);
			}
		},

		render_progress_target: function() {
			var progress_type = mybookprogress.progress_types[this.model.get('progress_type')];
			this.$('.mbp-phase-progress-target-units').text(progress_type.units);
			if(progress_type.hide_target_editor) { this.$('.mbp-phase-progress-target-setting').hide(); }
			else { this.$('.mbp-phase-progress-target-setting').show(); }
		},

		format_deadline: function(value) {
			if(value === null) { return ''; }
			return utils.format_date(value);
		},

		unformat_deadline: function(input, prev_value) {
			if(!input) { return null; }
			var date = utils.parse_date(input);
			if(typeof date === 'number') {
				this.$('.mbp-phase-deadline').removeClass('mbp-error');
				this.$('.mbp-phase-deadline-setting .mbp-setting-error').empty();
				return date;
			} else {
				this.$('.mbp-phase-deadline').addClass('mbp-error');
				this.$('.mbp-phase-deadline-setting .mbp-setting-error').text(date);
				return prev_value;
			}
		},

		update_progress_type: function(e) {
			this.model.change_progress_type(jQuery(e.target).attr('data-mbp-progress-type'));
		},

		unformat_progress_target: function(input, old_value) {
			if(!_.isFinite(parseInt(input))) { return old_value; }
			return Math.max(1, parseInt(input));
		},
	});

	/*---------------------------------------------------------*/
	/* Book Display                                            */
	/*---------------------------------------------------------*/

	mybookprogress.BookSetupDisplayView = View.extend({
		template: mybookprogress.utils.template(jQuery('#book_setup_display_step_template').html()),

		events: {
			'click .mbp-book-cover-image-button': 'clicked_cover_image',
		},

		initialize: function() {
			this.model.on('change:display_bar_color', this.render_colorpicker, this);
			this.model.on('change:display_cover_image', this.render_cover_image_button, this);

			settings.on('change:style_pack', this.render_preview, this);
			this.model.on('change:title', this.render_preview, this);
			this.model.on('change:phases', this.render_preview, this);
			this.model.on('change:display_bar_color', this.render_preview, this);
			this.model.on('change:display_cover_image', this.render_preview, this);
		},

		render: function() {
			this.$el.html(this.template());
			this.render_preview();
			this.render_controls();
			this.render_colorpicker();
			this.render_cover_image_button();
			return this;
		},

		render_preview: function() {
			var values = {
				style_pack: settings.get('style_pack'),
				title: this.model.get_title(),
				bar_color: this.model.get('display_bar_color'),
				cover_image: this.model.get('display_cover_image'),
				mbt_book: this.model.get('mbt_book'),
			};
			mybookprogress.WPQuery('mbp_get_preview', {values: JSON.stringify(values)}).then(_.bind(function(result) {
				this.$('.mbp-book-preview').html(result.output);
			}, this));
		},

		render_controls: function() {
			var barcolor = this.$('.mbp-book-bar-color');
			barcolor.colpick({layout: 'hex', submit: false, onChange: _.bind(this.barcolor_change, this)});
			barcolor.on('keydown', function(e) {
				if(e.keyCode === 8 || e.keyCode === 46) {
					return false;
				}
			});
			barcolor.on('keypress', function(e) {
				return false;
			});
			barcolor.on('paste', function(e) {
				var text;
				var clp = (e.originalEvent || e).clipboardData;
				if (clp === undefined || clp === null) {
					text = window.clipboardData.getData("text") || "";
				} else {
					text = clp.getData('text/plain') || "";
				}
				if(text !== "" && !/[a-fA-F0-9]{6}/.test(text)) {
					return false;
				}
			});
		},

		clicked_cover_image: function() {
			if(this.model.get('display_cover_image')) {
				this.model.set('display_cover_image', 0);
			} else {
				utils.media_selector(mybookprogress_i18n.cover_image, _.bind(this.cover_image_change, this));
			}
		},

		render_colorpicker: function() {
			var bar_color = this.model.get('display_bar_color');
			var color = bar_color.length == 6 ? bar_color : 'ffffff';
			var picker = this.$('.mbp-book-bar-color');
			picker.css('background', "#"+color);
			picker.css('color', utils.color_is_bright(color) ? 'black' : 'white');
			picker.colpickSetColor(color);
		},

		render_cover_image_button: function() {
			if(this.model.get('display_cover_image')) {
				this.$('.mbp-book-cover-image-button').addClass('has-image');
				this.$('.mbp-book-cover-image-button').text(mybookprogress_i18n.remove);
			} else {
				this.$('.mbp-book-cover-image-button').removeClass('has-image');
				this.$('.mbp-book-cover-image-button').text(mybookprogress_i18n.choose);
			}
		},

		barcolor_change: function(hsb, hex, rgb, el, bySetColor) {
			if(bySetColor) { return; }
			jQuery(el).val(hex);
			this.model.set('display_bar_color', hex);
		},

		cover_image_change: function(image) {
			this.model.set('display_cover_image', image['id']);
		},
	});

})();
