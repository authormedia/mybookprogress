(function () {
	mybookprogress.on('load_models', function() {
		mybookprogress.phase_templates = new mybookprogress.PhaseTemplates();
		mybookprogress.preload(mybookprogress.phase_templates.fetch());
	});

	mybookprogress.PhaseTemplate = mybookprogress.VirtualModel.extend({
		defaults: {
			name: '',
			phases: [],
			default: false,
		},
		sync: function(method, model, options) {
			setTimeout(_.bind(function(collection) { collection.save(); }, this, this.collection), 0);
			options.success();
		}
	});

	mybookprogress.PhaseTemplates = mybookprogress.WPCollection.extend({
		model: mybookprogress.PhaseTemplate,
		action: 'mbp_phase_templates',

		save: function() {
			this.sync('update', this);
		},
	});
})();
