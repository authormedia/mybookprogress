(function () {
	var blog_button = function(model) {
		return model.get('blog_post_id') ? mybookprogress_i18n.edit_blog_post : mybookprogress_i18n.make_blog_post;
	};

	var blog_share = function(model, button) {
		if(button.hasClass('mbp-disabled')) { return; }
		if(model.get('blog_post_id')) {
			model.verify_blog_post(function(verified) { if(!verified) { button.text(mybookprogress_i18n.make_blog_post); } });
			return window.open(window.location.href.substring(0, window.location.href.indexOf('wp-admin/')+9)+'post.php?post='+model.get('blog_post_id')+'&action=edit', '_blank');
		}
		button.addClass('mbp-disabled');

		var book = model.get_book();
		var phase = book.get_phase(model.get('phase_id'));
		var shortcode_attrs = {};
		shortcode_attrs.progress = model.get('progress');
		if(phase) { shortcode_attrs.phase_name = phase['name']; }
		if(phase && phase['deadline']) { shortcode_attrs.deadline = phase['deadline']; }
		shortcode_attrs.book = book.id;
		shortcode_attrs.book_title = book.get_title();
		shortcode_attrs.bar_color = book.get('display_bar_color');
		if(book.get('display_cover_image')) { shortcode_attrs.cover_image = book.get('display_cover_image'); }
		if(book.get('mbt_book')) { shortcode_attrs.mbt_book = book.get('mbt_book'); }
		var shortcode = '';
		for(attr in shortcode_attrs) {
			shortcode += ' '+attr+'="'+shortcode_attrs[attr]+'"';
		}
		shortcode = '[mybookprogress'+shortcode+']';

		mybookprogress.WPQuery('mbp_progress_sharing_blog', {title: model.get_book().get_title(), message: model.get('notes')+'\n'+shortcode}).then(function(response) {
			if(response === null || typeof response !== 'object' || 'error' in response) { response = {post_id: null}; }
			if(response.post_id) {
				window.open(window.location.href.substring(0, window.location.href.indexOf('wp-admin/')+9)+'post.php?post='+response.post_id+'&action=edit', '_blank');
				model.save({blog_post_id: response.post_id});
				button.text(mybookprogress_i18n.edit_blog_post);
			}
			button.removeClass('mbp-disabled');
		});
	};

	var twitter_share = function(model, button) {
		if(button.hasClass('mbp-disabled')) { return; }
		button.addClass('mbp-disabled');
		model.get_sharing_url(function(url) {
			button.removeClass('mbp-disabled');
			if(url) {
				window.open('https://twitter.com/intent/tweet?url='+encodeURIComponent(url)+'&text='+encodeURIComponent(model.get('notes')), '_blank');
			} else {
				window.open('https://twitter.com/intent/tweet?text='+encodeURIComponent(model.get('notes')), '_blank');
			}
		});
	};

	var facebook_share = function(model, button) {
		if(button.hasClass('mbp-disabled')) { return; }
		button.addClass('mbp-disabled');
		model.get_sharing_url(function(url) {
			button.removeClass('mbp-disabled');
			var title = model.get_book().get_title();
			title = (title ? title+' ' : '') + mybookprogress_i18n.progress_update;
			if(url) {
				window.open('http://www.facebook.com/dialog/feed?app_id=134530986736267&redirect_uri=http%3A%2F%2Fwww.facebook.com%2F&name='+encodeURIComponent(title)+'&link='+encodeURIComponent(url)+'&description='+encodeURIComponent(model.get('notes')), '_blank');
			} else {
				window.open('http://www.facebook.com/dialog/feed?app_id=134530986736267&redirect_uri=http%3A%2F%2Fwww.facebook.com%2F&name='+encodeURIComponent(title)+'&link='+encodeURIComponent(location.origin)+'&description='+encodeURIComponent(model.get('notes')), '_blank');
			}
		});
	};

	mybookprogress.progress_sharing_types = mybookprogress.progress_sharing_types || {};
	mybookprogress.progress_sharing_types.blog = {button: blog_button, share: blog_share, default: true};
	mybookprogress.progress_sharing_types.facebook = {button: mybookprogress_i18n.share_on_facebook, share: facebook_share, default: true};
	mybookprogress.progress_sharing_types.twitter = {button: mybookprogress_i18n.share_on_twitter, share: twitter_share, default: true};
})();
