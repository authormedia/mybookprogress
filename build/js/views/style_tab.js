(function () {
	var settings = null;
	var style_packs = null;
	var books = null;
	var utils = mybookprogress.utils;

	mybookprogress.on('models_loaded', function() {
		settings = mybookprogress.settings;
		style_packs = mybookprogress.style_packs;
		books = mybookprogress.books;
	});

	var View = mybookprogress.View;
	var CollectionView = mybookprogress.CollectionView;

	mybookprogress.StyleTabView = View.extend({
		className: 'mbp-admin-tab',
		template: mybookprogress.utils.template(jQuery('#style_tab_template').html()),

		events: {
			'click .mbp-upload-stylepack-button': 'upload_stylepack',
		},

		initialize: function() {
			this.set_subview('.mbp-style-packs', new mybookprogress.StylePacksView());
			this.set_subview('.mbp-book-style-sections', new mybookprogress.BookStylesView());
		},

		upload_stylepack: function() {
			utils.media_selector(mybookprogress_i18n.style_pack, _.bind(this.uploaded_stylepack, this));
		},

		uploaded_stylepack: function(pack) {
			mybookprogress.WPQuery('mbp_upload_stylepack', {stylepack_id: pack['id']}).then(_.bind(function(response) {
				style_packs.fetch();
			}, this));
		},
	});

	mybookprogress.StylePackView = View.extend({
		template: mybookprogress.utils.template(jQuery('#style_tab_style_pack_template').html()),

		events: {
			'click': 'activate',
		},

		initialize: function() {
			this.model.on('change:active', this.render_active, this);
		},

		render: function() {
			this.setElement(jQuery(this.template()));
			this.render_active();
			return this;
		},

		render_active: function() {
			if(this.model.get('active')) {
				this.$el.addClass('active');
			} else {
				this.$el.removeClass('active');
			}
		},

		activate: function() {
			if(!this.model.get('active')) {
				this.model.trigger('activate', this.model);
			}
		},
	});

	mybookprogress.StylePacksView = CollectionView.extend({
		item_view: mybookprogress.StylePackView,

		initialize: function(options) {
			this.collection = style_packs;
			CollectionView.prototype.initialize.call(this, options);

			this.style_pack = null;

			this.collection.each(function(pack) {
				if(pack.id === settings.get('style_pack')) {
					this.style_pack = pack;
					pack.set('active', true);
				} else {
					pack.set('active', false);
				}
			}, this);

			if(!this.style_pack) {
				this.style_pack = this.collection.at(0);
				this.style_pack.set('active', true);
				settings.set('style_pack', this.style_pack.id);
			}

			this.collection.on('activate', function(pack) {
				this.style_pack.set('active', false);
				this.style_pack = pack;
				this.style_pack.set('active', true);

				settings.set('style_pack', this.style_pack.id);
			}, this);
		},
	});

	mybookprogress.BookStyleView = View.extend({
		template: mybookprogress.utils.template(jQuery('#style_tab_book_style_template').html()),

		events: {
			'click .mbp-book-cover-image-button': 'clicked_cover_image',
		},

		initialize: function() {
			this.model.on('change:title', this.render_title, this);
			this.model.on('change:display_bar_color', this.render_colorpicker, this);
			this.model.on('change:display_cover_image', this.render_cover_image_button, this);

			settings.on('change:style_pack', this.render_preview, this);
			this.model.on('change:title', this.render_preview, this);
			this.model.on('change:phases', this.render_preview, this);
			this.model.on('change:display_bar_color', this.render_preview, this);
			this.model.on('change:display_cover_image', this.render_preview, this);
		},

		render: function() {
			this.$el.html(this.template());
			this.render_preview();
			this.render_controls();
			this.render_colorpicker();
			this.render_cover_image_button();
			this.render_title();
			return this;
		},

		render_title: function() {
			this.$('.mbp-section-header').text(this.model.get_title());
		},

		render_preview: function() {
			var values = {
				style_pack: settings.get('style_pack'),
				title: this.model.get_title(),
				bar_color: this.model.get('display_bar_color'),
				cover_image: this.model.get('display_cover_image'),
				mbt_book: this.model.get('mbt_book'),
			};
			mybookprogress.WPQuery('mbp_get_preview', {values: JSON.stringify(values)}).then(_.bind(function(result) {
				this.$('.mbp-book-preview').html(result.output);
			}, this));

			var style_pack = style_packs.get(settings.get('style_pack'));
			if(style_pack.get('supports').indexOf('bar-color') === -1) {
				this.$('.mbp-book-bar-color-setting').css('display', 'none');
			} else {
				this.$('.mbp-book-bar-color-setting').css('display', '');
			}
		},

		render_controls: function() {
			var barcolor = this.$('.mbp-book-bar-color');
			barcolor.colpick({layout: 'hex', submit: false, onChange: _.bind(this.barcolor_change, this)});
			barcolor.on('keydown', function(e) {
				if(e.keyCode === 8 || e.keyCode === 46) {
					return false;
				}
			});
			barcolor.on('keypress', function(e) {
				return false;
			});
			barcolor.on('paste', function(e) {
				var text;
				var clp = (e.originalEvent || e).clipboardData;
				if (clp === undefined || clp === null) {
					text = window.clipboardData.getData("text") || "";
				} else {
					text = clp.getData('text/plain') || "";
				}
				if(text !== "" && !/[a-fA-F0-9]{6}/.test(text)) {
					return false;
				}
			});
		},

		clicked_cover_image: function() {
			if(this.model.get('display_cover_image')) {
				this.model.set('display_cover_image', 0);
				this.model.save();
			} else {
				utils.media_selector(mybookprogress_i18n.cover_image, _.bind(this.cover_image_change, this));
			}
		},

		render_colorpicker: function() {
			var bar_color = this.model.get('display_bar_color');
			var color = bar_color.length == 6 ? bar_color : 'ffffff';
			var picker = this.$('.mbp-book-bar-color');
			picker.css('background', "#"+color);
			picker.css('color', utils.color_is_bright(color) ? 'black' : 'white');
			picker.colpickSetColor(color);
		},

		render_cover_image_button: function() {
			if(this.model.get('display_cover_image')) {
				this.$('.mbp-book-cover-image-button').addClass('has-image');
				this.$('.mbp-book-cover-image-button').text(mybookprogress_i18n.remove);
			} else {
				this.$('.mbp-book-cover-image-button').removeClass('has-image');
				this.$('.mbp-book-cover-image-button').text(mybookprogress_i18n.choose);
			}
		},

		barcolor_change: function(hsb, hex, rgb, el, bySetColor) {
			if(bySetColor) { return; }
			jQuery(el).val(hex);
			this.model.set('display_bar_color', hex);
			this.model.save();
		},

		cover_image_change: function(image) {
			this.model.set('display_cover_image', image['id']);
			this.model.save();
		},
	});

	mybookprogress.BookStylesView = CollectionView.extend({
		item_view: mybookprogress.BookStyleView,
		initialize: function(options) {
			this.collection = books;
			CollectionView.prototype.initialize.call(this, options);
		},
	});
})();
