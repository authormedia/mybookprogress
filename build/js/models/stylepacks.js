(function () {
	mybookprogress.on('load_models', function() {
		mybookprogress.style_packs = new mybookprogress.StylePacks();
		mybookprogress.preload(mybookprogress.style_packs.fetch());
	});

	mybookprogress.StylePack = mybookprogress.WPModel.extend({
		action: 'mbp_style_pack',
		defaults: {
			name: '',
			stylepack_uri: '',
			version: '',
			desc: '',
			author: '',
			author_uri: '',
			supports: [],
		}
	});

	mybookprogress.StylePacks = mybookprogress.WPCollection.extend({
		model: mybookprogress.StylePack,
		action: 'mbp_style_packs'
	});
})();
