(function () {
	mybookprogress.on('load_models', function() {
		mybookprogress.mbt_books = new mybookprogress.MyBookTable_Books();
		mybookprogress.preload(mybookprogress.mbt_books.fetch());
	});

	var MyBookTable_Book = mybookprogress.MyBookTable_Book = mybookprogress.WPModel.extend({
		action: 'mbp_mbt_book',
		defaults: {
			title: '',
			mbp_book: null,
		},
		set_book: function(mbp_book_id) {
			if(mbp_book_id !== this.get('mbp_book')) {
				this.save({mbp_book: mbp_book_id});
			}
		},
		get_title: function() {
			var title = this.get('title');
			return title ? title : '('+mybookprogress_i18n.no_title+')';
		},
	});

	var MyBookTable_Books = mybookprogress.MyBookTable_Books = mybookprogress.WPCollection.extend({
		model: mybookprogress.MyBookTable_Book,
		action: 'mbp_mbt_books',
		update_book: function(mbp_book) {
			var mbt_book_id = mbp_book.get('mbt_book');
			this.each(_.bind(function(book) {
				if(book.id === mbt_book_id) {
					book.set_book(mbp_book.id);
				} else if(book.get('mbp_book') === mbp_book.id) {
					book.set_book(null);
				}
			}, this));
		},
	});
})();
