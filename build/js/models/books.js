(function () {
	var phase_templates = null;
	var mbt_books = null;
	var utils = mybookprogress.utils;

	mybookprogress.on('models_loaded', function() {
		phase_templates = mybookprogress.phase_templates;
		mbt_books = mybookprogress.mbt_books;
	});

	var WPModel = mybookprogress.WPModel;
	var WPCollection = mybookprogress.WPCollection;

	mybookprogress.on('load_models', function() {
		mybookprogress.books = new mybookprogress.Books();
		mybookprogress.preload(mybookprogress.books.fetch());
	});

	mybookprogress.Book = WPModel.extend({
		action: 'mbp_book',
		defaults: {
			title: '',
			phases_status: {},
			phases: null,
			display_bar_color: 'CB3301',
			display_cover_image: 0,
			created: null,
			mbt_book: null,
		},

		initialize: function(model, options) {
			if(!this.get('created')) { this.set('created', utils.unix_timestamp_now()); }
			this.on('change', this.on_changed, this);
			this.on('sync', this.on_synced, this);
		},

		on_changed: function() {
			this.is_dirty = true;
		},

		on_synced: function() {
			this.is_dirty = false;
		},

		get_title: function() {
			var title = this.get('title');
			if(!title && this.get('mbt_book')) {
				var book = mbt_books.get(this.get('mbt_book'));
				if(book) { title = book.get('title'); }
			}
			return title ? title : mybookprogress_i18n.untitled_book;
		},

		get_progress: function() {
			if(!this.progress) {
				this.progress = new mybookprogress.BookProgress(null, {book_id: this.id});
				this.progress.fetch({reset: true});
				this.phases_progress = {};
				this.fetch_phases_progress();
				this.listenTo(this.progress, 'add change remove', this.schedule_fetch_phases_progress);
			}
			return this.progress;
		},

		get_phases: function() {
			var phases = this.get('phases');
			if(typeof phases === 'string') {
				var template = phase_templates.get(phases);
				phases = template ? template.get('phases') : null;
			}
			if(!phases) { phases = []; }
			return jQuery.extend(true, [], phases);
		},

		get_phase: function(phase_id) {
			var phases = this.get_phases();
			for(var i = phases.length - 1; i >= 0; i--) {
				if(phases[i].id == phase_id) {
					return phases[i];
				}
			}
		},

		update_phase: function(phase) {
			var phases = this.get_phases();
			for(var i = phases.length - 1; i >= 0; i--) {
				if(phases[i].id == phase.id && !_.isEqual(phases[i], phase)) {
					phases[i] = _.clone(phase);
					break;
				}
			}
			this.set('phases', phases);
		},

		get_phase_status: function(phase_id) {
			var phases_status = this.get('phases_status');
			return phases_status[phase_id] ? phases_status[phase_id] : '';
		},

		update_phase_status: function(phase_id, status) {
			var phases_status = jQuery.extend(true, {}, this.get('phases_status'));
			phases_status[phase_id] = status;
			this.set('phases_status', phases_status);
		},

		get_phase_progress: function(phase_id) {
			var phases_progress = this.get_phases_progress();
			return phases_progress[phase_id] ? phases_progress[phase_id] : 0;
		},

		get_phases_progress: function(phase_id) {
			if(!this.phases_progress) {
				this.get_progress();
			}
			return this.phases_progress;
		},

		schedule_fetch_phases_progress: function(model) {
			this.stopListening(model);
			this.listenToOnce(model, 'sync', _.bind(function() { this.fetch_phases_progress() }, this));
		},

		fetch_phases_progress: function(callback) {
			mybookprogress.WPQuery('mbp_get_book_phases_progress', {book_id: this.id}).then(_.bind(function(response) {
				if(response === null || typeof response !== 'object' || 'error' in response) { response = null; }
				if(response) { this.phases_progress = response; this.trigger('change:phases_progress'); }
			}, this));
		},

		get_next_phase: function() {
			var first_uncomplete = null;
			var first_working = null;

			var phases = this.get_phases();
			for(var i = 0; i < phases.length; i++) {
				var phase_status = this.get_phase_status(phases[i].id);
				var phase_progress = this.get_phase_progress(phases[i].id);
				if(phase_status !== 'complete') {
					if(!first_uncomplete) { first_uncomplete = phases[i]; }
					if(phase_progress > 0) {
						first_working = phases[i];
						break;
					}
				}
			}

			return first_working ? first_working : first_uncomplete;
		},

		is_complete: function() {
			var phases = this.get_phases();
			for(var i = phases.length - 1; i >= 0; i--) {
				if(this.get_phase_status(phases[i].id) !== 'complete') { return false; }
			}
			return true;
		},
	});

	mybookprogress.Books = WPCollection.extend({
		model: mybookprogress.Book,
		action: 'mbp_books'
	});
})();
