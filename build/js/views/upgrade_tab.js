(function () {
	var settings = null;

	mybookprogress.on('models_loaded', function() {
		settings = mybookprogress.settings;
	});

	var View = mybookprogress.View;

	mybookprogress.UpgradeTabView = View.extend({
		className: 'mbp-admin-tab',
		template: mybookprogress.utils.template(jQuery('#upgrade_tab_template').html()),

		events: {
			'click .mbp-upgrade-upsell-button': 'upgrade',
			'click .mbp-upgrade-moreinfo-button': 'moreinfo',
			'click .mbp-alert-message': 'click_alert',
		},

		initialize: function() {
			this.set_subview('.mbp-apikey-section', new mybookprogress.APIKeyView());
			this.listenTo(settings, 'change:upgrade_enabled change:upgrade_exists', this.detect_alert);
			setTimeout(_.bind(function() { this.detect_alert(); }, this), 0);
		},

		render: function() {
			View.prototype.render.call(this);
			this.render_alert();
			return this;
		},

		render_alert: function() {
			if(this.alert) {
				if(!jQuery('.mbp-admin-tabs > .ui-tabs-nav a[href="#mbp-upgrade-tab"] .mbp-alert').length) {
					jQuery('.mbp-admin-tabs > .ui-tabs-nav a[href="#mbp-upgrade-tab"]').append(jQuery('<div class="mbp-alert"></div>'));
				}
				if(this.alert == 'no_upgrade_plugin') {
					this.$('.mbp-alert-message').html(mybookprogress_i18n.upgrade_plugin_not_installed_error);
					this.$('.mbp-alert-message').removeClass('mbp-disabled');
				} else if(this.alert == 'no_apikey') {
					this.$('.mbp-alert-message').html(mybookprogress_i18n.problem_with_apikey_error);
					this.$('.mbp-alert-message').addClass('mbp-disabled');
				}
				this.$('.mbp-alert-message-container').show();
			} else {
				jQuery('.mbp-admin-tabs > .ui-tabs-nav a[href="#mbp-upgrade-tab"] .mbp-alert').remove();
				this.$('.mbp-alert-message-container').hide();
			}
		},

		click_alert: function() {
			if(this.alert == 'no_upgrade_plugin') {
				window.location.href = window.location.href.substring(0, window.location.href.indexOf('wp-admin/')+9)+'admin.php?page=mybookprogress&subpage=mbp_get_upgrade_page';
			}
		},

		detect_alert: function() {
			if(settings.get('upgrade_enabled') !== false && settings.get('upgrade_exists') !== settings.get('upgrade_enabled')) {
				this.alert = 'no_upgrade_plugin';
			} else if(settings.get('upgrade_enabled') === false && settings.get('upgrade_exists') !== false) {
				this.alert = 'no_apikey';
			} else {
				this.alert = '';
			}

			this.render_alert();
		},

		upgrade: function() {
			mbp_track_event('upgrade_tab_upgrade_button_clicked');
			window.open('https://gumroad.com/l/MyBookProgressPro', '_blank').focus();
		},

		moreinfo: function() {
			mbp_track_event('upgrade_tab_moreinfo_button_clicked');
			window.open('http://www.authormedia.com/all-products/mybookprogress/pricing/', '_blank').focus();
		},
	});

	mybookprogress.APIKeyView = View.extend({
		template: mybookprogress.utils.template(jQuery('#upgrade_apikey_template').html()),

		events: {
			'input .mbp-apikey': 'changed_apikey',
			'click .mbp-apikey-setting .mbp-setting-feedback': 'clicked_feedback',
			'click .mbp-upgrade-enter-apikey-button': 'enter_apikey',
		},

		initialize: function() {
			this.verified_apikey();
			this.has_apikey = (settings.get('apikey') !== '' || settings.get('upgrade_exists') !== false);
		},

		render: function() {
			View.prototype.render.call(this);
			this.$('.mbp-apikey').val(settings.get('apikey'));
			this.render_view();
			this.render_feedback();
			return this;
		},

		render_view: function() {
			if(this.has_apikey) {
				this.$('.mbp-upgrade-buynow').hide();
				this.$('.mbp-upgrade-enter-apikey').show();
			} else {
				this.$('.mbp-upgrade-buynow').show();
				this.$('.mbp-upgrade-enter-apikey').hide();
			}
		},

		enter_apikey: function() {
			this.has_apikey = true;
			this.render_view();
		},

		clicked_feedback: function() {
			if(this.apikey_status == 'editing') {
				settings.set({apikey: this.$('.mbp-apikey').val()}, {no_save: true});
				this.verify_apikey();
			} else if(this.apikey_status == 'empty') {
				window.open('http://www.mybookprogress.com', '_blank').focus();
			}
		},

		changed_apikey: function() {
			this.apikey_status = 'editing';
			this.render_feedback();
		},

		verify_apikey: function() {
			this.apikey_status = 'checking';
			mybookprogress.WPQuery('mbp_update_apikey', {apikey: settings.get('apikey')}).then(_.bind(this.verified_apikey, this));
			this.render_feedback();
		},

		verified_apikey: function(response) {
			if(typeof response !== 'undefined' && response !== null && typeof response === 'object' && !('error' in response)) {
				settings.set({
					apikey_status: response.apikey_status,
					apikey_message: response.apikey_message,
					upgrade_enabled: response.upgrade_enabled,
					upgrade_exists: response.upgrade_exists,
				}, {no_save: true});
			}

			this.apikey_status = settings.get('apikey_status') == 0 ? 'empty' : (settings.get('apikey_status') > 0 ? 'good' : 'bad');
			this.render_feedback();
		},

		render_feedback: function() {
			this.$('.mbp-apikey').attr('class', 'mbp-apikey');
			this.$('.mbp-apikey').removeAttr('disabled');
			this.$('.mbp-apikey-setting .mbp-setting-feedback').attr('class', 'mbp-setting-feedback');
			if(this.apikey_status == 'checking') {
				this.$('.mbp-apikey').attr('disabled', 'disabled');
				this.$('.mbp-apikey-setting .mbp-setting-feedback').addClass('checking');
			} else if(this.apikey_status == 'good') {
				this.$('.mbp-apikey').addClass('mbp-correct');
				this.$('.mbp-apikey-setting .mbp-setting-feedback').addClass('good');
			} else if(this.apikey_status == 'bad') {
				this.$('.mbp-apikey').addClass('mbp-error');
				this.$('.mbp-apikey-setting .mbp-setting-feedback').addClass('bad');
			} else if(this.apikey_status == 'editing') {
				this.$('.mbp-apikey-setting .mbp-setting-feedback').addClass('refresh');
			} else if(this.apikey_status == 'empty') {
				this.$('.mbp-apikey-setting .mbp-setting-feedback').addClass('help');
			}
		},
	});

})();
