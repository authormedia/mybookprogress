import subprocess

files = [
	'admin.js',
	'utility.js',
	'progress_types.js',
	'progress_sharing.js',
	'models/general.js',
	'models/mbt_books.js',
	'models/books.js',
	'models/phasetemplates.js',
	'models/progress.js',
	'models/settings.js',
	'models/stylepacks.js',
	'views/general.js',
	'views/admin.js',
	'views/help.js',
	'views/sir_walter.js',
	'views/book_progress.js',
	'views/book_setup.js',
	'views/display_tab.js',
	'views/progress_tab.js',
	'views/promote_tab.js',
	'views/style_tab.js',
	'views/upgrade_tab.js',
]

output = '../../js/admin.js'

command = 'cat '+' '.join(files)+' > '+output

subprocess.call(command, shell=True)