(function () {
	var WPSync = mybookprogress.WPSync = function(method, model, options) {
		options = options || {};

		var data = null;
		switch(method) {
			case 'create':
			case 'update':
				data = model;
				break;
			case 'read':
			case 'delete':
				data = {id: model.id};
				break;
			case 'patch':
				data = jQuery.extend({id: model.id}, options.attrs);
				break;
		}
		if(!data) {
			if(options.error) { options.error(); }
			return false;
		}

		var query = mybookprogress.WPQuery(this.action+'_'+method, jQuery.extend({object: JSON.stringify(data)}, options.extra_data));
		query.done(function(response) {
			if(response === null || typeof response !== 'object') {
				if(options.error) { options.error('invalid response'); }
				return;
			}
			if('error' in response) {
				if(options.error) { options.error(response.error); }
			} else {
				switch(method) {
					case 'create':
						model.set('id', response.id);
						if(options.success) { options.success(); }
						break;
					case 'read':
						if(options.success) { options.success(response.object); }
						break;
					case 'patch':
					case 'update':
					case 'delete':
						if(options.success) { options.success(); }
						break;
					default:
						if(options.error) { options.error(); }
						break;
				}
			}
		});
		query.fail(function() {
			if(options.error) { options.error(); }
		});
		return query;
	};

	mybookprogress.WPModel = Backbone.Model.extend({
		sync: WPSync
	});

	mybookprogress.WPCollection = Backbone.Collection.extend({
		sync: WPSync
	});

	mybookprogress.VirtualModel = Backbone.Model.extend({
		sync: function(method, model, options) { options.success(); }
	});

	mybookprogress.VirtualCollection = Backbone.Collection.extend({
		sync: function(method, model, options) { options.success(); }
	});
})();
