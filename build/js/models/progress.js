(function () {
	var books = null;
	var settings = null;

	mybookprogress.on('models_loaded', function() {
		books = mybookprogress.books;
		settings = mybookprogress.settings;
	});

	var WPModel = mybookprogress.WPModel;
	var WPCollection = mybookprogress.WPCollection;

	mybookprogress.ProgressEntry = WPModel.extend({
		action: 'mbp_progress_entry',
		defaults: {
			book_id: 0,
			phase_id: 0,
			phase_name: '',
			phase_complete: false,
			timestamp: 0,
			progress_type: '',
			progress: null,
			target: null,
			notes: '',
		},

		initialize: function() {
			if(this.collection) { this.set('book_id', this.collection.book_id); }
			this.check_phase_name();
			this.listenTo(this.get_book(), 'change:phases', this.check_phase_name);
		},

		check_phase_name: function() {
			var phase = this.get_book().get_phase(this.get('phase_id'));
			if(phase) {
				if(phase.name !== this.get('phase_name')) {
					this.set('phase_name', phase.name);
					if(!this.isNew()) { this.save(); }
				}
			}
		},

		get_book: function() {
			return books.get(this.get('book_id'));
		},

		get_sharing_url: function(callback) {
			var post_id = this.get('blog_post_id');
			if(!post_id && settings.get('mybooktable_social_media_link')) {
				var book = this.get_book();
				if(book && book.get('mbt_book')) {
					post_id = book.get('mbt_book');
				}
			}

			if(post_id) {
				mybookprogress.WPQuery('mbp_get_post_permalink', {post_id: post_id}).then(function(response) {
					if(typeof response !== 'string') { response = null; }
					callback(response);
				});
			} else {
				callback(null);
			}

			this.verify_blog_post();
		},

		verify_blog_post: function(callback) {
			callback = callback || function(){};
			var post_id = this.get('blog_post_id');
			if(!post_id) { return callback(false); }
			var self = this;
			mybookprogress.WPQuery('mbp_get_post_permalink', {post_id: post_id}).then(function(response) {
				if(typeof response === 'string') {
					callback(true);
				} else {
					self.save({'blog_post_id': null});
					callback(false);
				}
			});
		},
	});

	mybookprogress.BookProgress = WPCollection.extend({
		model: mybookprogress.ProgressEntry,
		action: 'mbp_book_progress',
		comparator: function(m) { return m.mbp_is_new ? -Math.pow(10, 10) : -m.get('timestamp'); },

		initialize: function(models, options) {
			if(typeof options.book_id !== 'undefined') { this.book_id = options.book_id; }
			if(!this.book_id) { throw 'must provide book id'; }

			this.last = 0;
			this.has_more = true;
		},

		get_book: function() {
			return books.get(this.book_id);
		},

		next_page: function() {
			if(this.fetching || !this.has_more_pages()) { return; }
			this.fetching = true;
			this.fetch({success: _.bind(function() { this.fetching = false; }, this)});
		},

		has_more_pages: function() {
			return this.has_more;
		},

		sync: function(method, model, options) {
			options.remove = false;
			var query = mybookprogress.WPQuery(this.action+'_'+method, {book_id: this.book_id, before: this.last});
			query.done(_.bind(function(response) {
				if(response === null || typeof response !== 'object') {
					options.error('invalid response');
					return;
				}
				if('error' in response) {
					options.error(response.error);
				} else {
					switch(method) {
						case 'read':
							this.last = response.object.last;
							this.has_more = response.object.has_more;
							if(options.success) { options.success(response.object.entries); }
							break;
						default:
							options.error();
							break;
					}
				}
			}, this));
			query.fail(function() {
				options.error();
			});
			return query;
		},
	});
})();
