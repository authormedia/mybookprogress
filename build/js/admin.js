(function () {
	var MyBookProgress = function() {};
	_.extend(MyBookProgress.prototype, Backbone.Events, {
		preload: function(promise) {
			if(!this.preload_list) { this.preload_list = []; }
			this.preload_list.push(promise);
		},
		do_preload: function(argument) {
			return jQuery.when.apply(jQuery, this.preload_list);
		}
	});
	window.mybookprogress = new MyBookProgress();

	jQuery(document).ready(function() {
		jQuery('#wpbody').mbp_loading();
		mybookprogress.preload(mybookprogress.utils.google_load("visualization", "1.0", {"packages":["corechart"]}));
		mybookprogress.trigger('load_models');
		mybookprogress.do_preload().done(function() {
			jQuery('#wpbody').mbp_loading('destroy');
			mybookprogress.trigger('models_loaded');
			mybookprogress.admin = new mybookprogress.AdminView();
			mybookprogress.trigger('loaded');
		});
	});
})();
