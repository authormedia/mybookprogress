(function () {
	var books = null;

	mybookprogress.on('models_loaded', function() {
		books = mybookprogress.books;
	});

	var WPModel = mybookprogress.WPModel;

	mybookprogress.on('load_models', function() {
		mybookprogress.settings = new mybookprogress.Settings();
		mybookprogress.preload(mybookprogress.settings.fetch());
	});

	mybookprogress.Settings = WPModel.extend({
		action: 'mbp_settings',

		initialize: function() {
			this.on('change', this.onchange, this);
		},

		onchange: function(model, options) {
			if(!options || !options.no_save) { this.save(model.changedAttributes(), {patch: true}); }
		},

		fetch: function(options) {
			return WPModel.prototype.fetch.call(this, jQuery.extend(options, {no_save: true}));
		},
	});
})();
