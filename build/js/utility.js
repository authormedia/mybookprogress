(function () {
	var settings = null;

	mybookprogress.on('models_loaded', function() {
		settings = mybookprogress.settings;
	});

	var Utility = function() {};
	_.extend(Utility.prototype, {
		capitalize: function(string) {
			return string.charAt(0).toUpperCase() + string.slice(1);
		},

		uncapitalize: function(string) {
			return string.toLowerCase();
		},

		color_is_bright: function(color) {
			var rgbcolor = jQuery.colpick.hexToRgb(color);
			var luma = 0.2126 * rgbcolor.r + 0.7152 * rgbcolor.g + 0.0722 * rgbcolor.b;
			return luma > 128;
		},

		media_selector: function(title, callback) {
			if(!this._media_frame) {
				this._media_frame = wp.media.frames.media_selector = wp.media({
					multiple: false
				});
			}

			var callback_wrapper = function() {
				attachment = this._media_frame.state().get('selection').first().toJSON();
				callback(attachment);
			}

			var callback_remover = function() {
				setTimeout(_.bind(function() { this._media_frame.off('select', callback_wrapper); }, this), 0);
				this._media_frame.off('close', callback_remover);
			}

			this._media_frame.on('select', _.bind(callback_wrapper, this));
			this._media_frame.on('close', _.bind(callback_remover, this));

			this._media_frame.open();

			this._media_frame.state().set('title', title)._title();
		},

		modal: function(content, options) {
			if(!this.modal_id_counter) {
				this.modal_id_counter = 0;
			}

			var holder = jQuery('<div/>');
			holder.addClass('mbp-modal-holder');
			holder.attr('id', 'mbp-modal-'+this.modal_id_counter++);
			jQuery('#mbp-admin-page').append(holder);
			holder.append(content);

			var options = jQuery.extend({
				title: '',
				width: 600,
				height: holder.height()+5,
			}, options);

			holder.css('display', 'none');

			var old_tb_remove = window.tb_remove;
			window.tb_remove = function() {
				window.tb_remove = old_tb_remove;
				window.tb_remove(window);
				if(options.close) { options.close(); }
			};

			var width_units = 'px', height_units = 'px';
			if(typeof options.height === 'string') {
				if(options.height.indexOf('%') !== -1) { height_units = '%'; }
				options.height = parseFloat(options.height.replace(/[^0-9\.]/g, ''));
			}
			if(typeof options.width === 'string') {
				if(options.width.indexOf('%') !== -1) { width_units = '%'; }
				options.width = parseFloat(options.width.replace(/[^0-9\.]/g, ''));
			}

			tb_show(options.title, '#TB_inline?inlineId='+holder.attr('id'));

			if(height_units === '%') {
				jQuery('#TB_window').css({'margin-top': 0, 'top': (50-options.height*0.5).toString()+'%', 'height': options.height.toString()+'%'});
				jQuery('#TB_ajaxContent').css({'height': 'auto', 'position': 'absolute', 'top': '30px', 'bottom': '0'});
			} else {
				jQuery('#TB_window').css({'margin-top': '-'+(options.height*0.5).toString()+height_units});
				jQuery('#TB_ajaxContent').css({'height': options.height.toString()+height_units});
			}
			if(width_units === '%') {
				jQuery('#TB_window').css({'margin-left': 0, 'left': (50-options.width*0.5).toString()+'%', 'width': options.width.toString()+'%'});
			} else {
				jQuery('#TB_window').css({'margin-left': '-'+(options.width*0.5).toString()+width_units, 'width': options.width.toString()+width_units});
			}
			jQuery('#TB_ajaxContent').css({'width': '100%', 'padding': 0});

			holder.remove();
		},

		do_mailchimp_query: function(method, data) {
			return mybookprogress.WPQuery('mbp_do_mailchimp_query', {apikey: settings.get('mailchimp_apikey'), method: method, data: JSON.stringify(data || {})});
		},

		datepicker: function(element, options) {
			element.datepicker(jQuery.extend({
				dateFormat: settings.get('date_format'),
				beforeShow: function(input, inst) { jQuery('#ui-datepicker-div').addClass('mbp-ui-datepicker'); },
			}, options));
		},

		tooltip: function(element, message, options) {
			element.tooltip(jQuery.extend({
				tooltipClass: 'mbp-ui-tooltip',
				items: element,
				content: message,
				position: {
					my: "left top+15",
					at: "left+10 bottom",
					collision: "none",
					using: function(position, feedback) {
						jQuery(this).css(position);
						jQuery("<div>").addClass("arrow left top").appendTo(this);
					}
				},
			}, options));
		},

		pointer: function(content, options) {
			if(!this.pointer_id_counter) {
				this.pointer_id_counter = 0;
			}
			var id = this.pointer_id_counter++;

			jQuery('#wpadminbar').pointer(jQuery.extend({
				position: {edge: 'top', align: 'center'},
				buttons: function() {}
			}, options, {
				pointerClass: 'mbp-pointer mbp-pointer-'+id + (options.pointerClass ? ' '+options.pointerClass : ''),
				content: ' ',
			})).pointer('open');
			jQuery('.mbp-pointer-'+id+' .wp-pointer-content').empty().append(content);
		},

		utcdate: function(date) {
			return {
				getDate: date.getUTCDate.bind(date),
				getDay: date.getUTCDay.bind(date),
				getFullYear: date.getUTCFullYear.bind(date),
				getHours: date.getUTCHours.bind(date),
				getMilliseconds: date.getUTCMilliseconds.bind(date),
				getMinutes: date.getUTCMinutes.bind(date),
				getMonth: date.getUTCMonth.bind(date),
				getSeconds: date.getUTCSeconds.bind(date),
				getTime: date.getTime.bind(date),
			};
		},

		format_date: function(date) {
			if(typeof date === 'number') { date = new Date(date*1000); }
			return jQuery.datepicker.formatDate(settings.get('date_format'), this.utcdate(date));
		},

		parse_date: function(date) {
			try {
				var date = jQuery.datepicker.parseDate(settings.get('date_format'), date);
				return Date.UTC(date.getFullYear(),date.getMonth(),date.getDate())*0.001;
			} catch(e) {
				return e;
			}
		},

		unix_timestamp: function(date) {
			return date.getTime()*0.001;
		},

		unix_timestamp_now: function() {
			return this.unix_timestamp(new Date());
		},


		month_name: function(month) {
			var months = [
				mybookprogress_i18n.january,
				mybookprogress_i18n.february,
				mybookprogress_i18n.march,
				mybookprogress_i18n.april,
				mybookprogress_i18n.may,
				mybookprogress_i18n.june,
				mybookprogress_i18n.july,
				mybookprogress_i18n.august,
				mybookprogress_i18n.september,
				mybookprogress_i18n.october,
				mybookprogress_i18n.november,
				mybookprogress_i18n.december,
			];

			return months[month];
		},

		human_time_diff: function(start, finish) {
			var diff = Math.abs(start-finish);

			var DAY_IN_SECONDS = 24*60*60;

			if(diff < 31*DAY_IN_SECONDS) {
				return Math.floor(diff/DAY_IN_SECONDS)+' '+mybookprogress_i18n.days;
			} else if(diff < 90*DAY_IN_SECONDS) {
				return Math.floor(diff/(7*DAY_IN_SECONDS))+' '+mybookprogress_i18n.weeks;
			} else {
				return Math.floor(diff/(30*DAY_IN_SECONDS))+' '+mybookprogress_i18n.months;
			}
		},

		google_load: function(moduleName, moduleVersion, optionalSettings) {
			var defer = jQuery.Deferred();

			var callback = function() { defer.resolve(); }
			optionalSettings.callback = callback;

			google.load(moduleName, moduleVersion, optionalSettings);

			return defer.promise();
		},

		progress_format: function(progress) {
			if(progress >= 10) { return this.number_format(progress); }
			return this.number_format(progress, 1);
		},

		number_format: function(number, decimal_places) {
			if(typeof decimal_places === 'undefined') { decimal_places = 0; }
			var formatted_number = number.toFixed(decimal_places);
			if(decimal_places > 0) {
				while(formatted_number.slice(-1) == '0') { formatted_number = formatted_number.slice(0, -1); }
				if(formatted_number.slice(-1) == '.') { formatted_number = formatted_number.slice(0, -1); }
			}
			return formatted_number;
		},

		template: function(template, data) {
			var data = jQuery.extend({
				evaluate: /\{\{(.+?)\}\}/g,
				interpolate: /\{\{=(.+?)\}\}/g,
				escape: /\{\{-(.+?)\}\}/g,
			}, data);
			return _.template(template, data);
		},

		get_query_var: function(variable) {
			var query = window.location.search.substring(1);
			var vars = query.split("&");
			for(var i = 0; i<vars.length; i++) {
				var pair = vars[i].split("=");
				if(pair[0] == variable) { return pair[1]; }
			}
			return false;
		},
	});
	var utils = mybookprogress.utils = new Utility();

	mybookprogress.WPQuery = function(action, data) {
		console.log('action', action, data);
		var defer = jQuery.Deferred();

		var query = jQuery.post(ajaxurl, jQuery.extend({}, data, {action: action}));
		query.done(function(response) {
			console.log('response', action, response);
			var result = null;
			if(response === '') {
				result = {};
			} else {
				try {
					result = JSON.parse(response);
				} catch(e) {
					defer.reject(e);
					return;
				}
			}
			defer.resolve(result);
		});
		query.fail(function(e) {
			defer.reject(e);
		});

		return defer.promise();
	};

	var mbp_loading_num = 1;
	jQuery.widget('custom.mbp_loading', {
		options: {},
		_create: function() {
			if(this.element.data('mbp_loading_data')) { return; }
			this.element.data('mbp_loading_data', jQuery('<div class="mbp-loading" id="mbp-loading-'+(mbp_loading_num++)+'"></div>').appendTo(document.body).css({
				position: 'absolute',
				left: this.element.offset().left,
				top: this.element.offset().top,
				height: this.element.outerHeight(),
				width: this.element.outerWidth(),
			}));
		},
		_destroy: function() {
			this.element.data('mbp_loading_data').remove();
			this.element.removeData('mbp_loading_data');
		}
	});
})();
